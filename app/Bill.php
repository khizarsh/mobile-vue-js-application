<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'bill_id';

    public function Supplier()
    {
    	return $this->hasOne('App\Supplier','supplier_id','supplier_id');
    }

    public function Term()
    {
    	return $this->hasOne('App\Term','term_id','term_id');
    }

    public function Tax()
    {
        return $this->hasOne('App\Tax','tax_id','tax_id');
    }

    public function BillProducts()
    {
    	return $this->hasMany('App\BillProduct','bill_id','bill_id')->with('Tax','Category');
    }

    public function SubTotal($bill_id)
    {
        $sub_total = round(BillProduct::where('bill_id',$bill_id)->sum('amount'),2);
        return $sub_total;
    }

    public function Discount($bill_id)
    {
        $discount = round(BillProduct::where('bill_id',$bill_id)->sum('discount_amount'),2);
        return $discount;
    }

    public function TotalTax($bill_id)
    {
        $total_tax = round(BillProduct::where('bill_id',$bill_id)->sum('tax_amount'),2);
        return $total_tax;
    }

    public function Taxes($bill_id)
    {
        return BillProduct::where('bill_id',$bill_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }

    public function TaxableValue($bill_id,$tax_id)
    {
        $amount =  round(BillProduct::where([['bill_id',$bill_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(BillProduct::where([['bill_id',$bill_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

        //return round($amount-$discount);
        return round($amount);
    }

    // public function BillPaymentTotal(){
    //     return $this->hasOne('App\PaymentParticular','reference_id','bill_id')
    //                 ->selectRaw('reference_id,SUM(paid_amount) as paid_amount')
    //                 ->where('reference','App\Bill')
    //                 ->groupBy('reference_id');
    // }

    public function BillTax(){
        return $this
                    ->selectRaw('bill_id,tax_id,SUM(amount)-SUM(discount_amount) as amount,SUM(tax_amount)/2 as cgst_amount,SUM(tax_amount)/2 as sgst_amount,SUM(tax_amount) as igst_amount,SUM(discount_amount) as discount_amount,SUM(sub_total) as sub_total,SUM(quantity) as quantity')
                    ->with('Tax')
                    ->groupBy(['bill_id','tax_id']);
    }
  
}


         
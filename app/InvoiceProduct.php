<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceProduct extends Model
{

    protected $guarded = [];

    protected $primaryKey = 'invoice_product_id';


    public function Invoice()
    {
        return $this->belongsTo('App\Invoice','invoice_id','invoice_id')->with('Customer');
    }

    public function Product()
    {
        return $this->hasOne('App\Product','product_id','product_id');
    }

    public function Category()
    {
        return $this->hasOne('App\Category','category_id','category_id');
    }
}

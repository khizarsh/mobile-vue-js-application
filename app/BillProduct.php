<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillProduct extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'bill_id';


    public function Bill()
    {
        return $this->belongsTo('App\Bill','bill_id','bill_id');
    }

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id');
    }

    public function Category()
    {
        return $this->hasOne('App\Category','category_id','category_id');
    }
}

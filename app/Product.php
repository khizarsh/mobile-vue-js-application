<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Customer;

class Product extends Model 
{

    protected $guarded = [];

    protected $primaryKey = 'product_id';

    public function Category()
    {
        return $this->belongsTo('App\Category','category_id','category_id');
    }

    public function Supplier()
    {
        return $this->belongsTo('App\Supplier','supplier_id','supplier_id');
    }
    
}

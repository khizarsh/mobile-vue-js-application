<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
	use Notifiable;

    protected $guarded = [];

    protected $primaryKey = 'customer_id';

    protected $hidden = [
        'password', 'remember_token',
    ];
}
<?php

namespace App\Http\Controllers;

use PDF;
use App\Bill;
use App\Invoice;
use App\Supplier;
use App\Employee;
use App\Customer;
use App\Category;
use App\Payment;
use App\Receipt;
use App\Proforma;
use App\Quotation;
use App\Indent;
use App\Subcategory;
use App\IndentProduct;
use App\Product;
use App\BillProduct;
use App\Organization;
use Illuminate\Http\Request;

class ReportController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function stock_report(Request $request)
    {
        $stock_products = [];
        $category       = '';
        $subcategory    = '';
        $products       = Product::get();
        $org         	= Organization::first(); 
        $to_date     	= date('Y-m-d',strtotime($request->to_date));
        $from_date   	= date('Y-m-d',strtotime($request->from_date));
        if($request->subcategory_id!='' && $request->category_id!=''){
            $products   = Product::where([['category_id',$request->category_id],
                                    ['subcategory_id',$request->subcategory_id]])
                                    ->get();
            $category      = Category::where('category_id',$request->category_id)->first();
            $subcategory   = Subcategory::where('subcategory_id',$request->subcategory_id)->first();

        }elseif ($request->category_id!='') {
            $products   = Product::where('category_id',$request->category_id)->get();
            $category   = Category::where('category_id',$request->category_id)->first();

        }elseif ($request->subcategory_id!='') {
            $products   = Product::where('subcategory_id',$request->subcategory_id)->get();
            $subcategory   = Subcategory::where('subcategory_id',$request->subcategory_id)->first();
        }
       
        foreach ($products as $product) 
        {
            $data = [
                'product' => $product,
                'pieces_opening_stock'     => 0,
                'weight_opening_stock'     => 0,
                'pieces_purchase_stock'    => 0,
                'weight_purchase_stock'    => 0,
                'pieces_indent_stock'      => 0,
                'weight_indent_stock'      => 0,
                'weight_clossing_stock'    => 0,
                'pieces_clossing_stock'    => 0,
            ];
            
            if($product->unit=='Pieces')
            {
                $purchase_pieces_stock = Billproduct::
                            where([['product_id',$product->product_id],['unit','Pieces']])
                            ->whereHas('Bill',function($value) use($from_date,$to_date){
                            $value->whereBetween('bill_date',[$from_date,$to_date]);
                            })->sum('pieces');
                $opening_pieces_purchase = BillProduct::
                            where([['product_id',$product->product_id],['unit','Pieces']])
                            ->whereHas('Bill',function($value) use($request){
                            $value->whereDate('bill_date','<',$request->from_date);
                            })->sum('pieces');

                $opening_pieces_indent = IndentProduct::
                            where([['product_id',$product->product_id],['unit','Pieces']])
                            ->whereHas('Indent',function($value) use($request){
                            $value->whereDate('indent_date','<',$request->from_date);
                            })->sum('quantity');

                $indent_pieces_stock = IndentProduct::
                            where([['product_id',$product->product_id],['unit','Pieces']])
                            ->whereHas('Indent',function($value) use($from_date,$to_date){
                            $value->whereBetween('indent_date',[$from_date,$to_date]);
                            })->sum('quantity');
               
                $data['pieces_opening_stock']  = $product->opening_stock+$opening_pieces_purchase-
                                                  $opening_pieces_indent;
                $data['weight_opening_stock']  =  0;
                $data['pieces_purchase_stock'] = $purchase_pieces_stock;
                $data['pieces_indent_stock']   = $indent_pieces_stock;
               
                $data['pieces_clossing_stock']  = ($purchase_pieces_stock + $data['pieces_opening_stock']) -
                                                $indent_pieces_stock;
            }
            else
            {
                    $opening_weight_purchase = BillProduct::
                    where([['product_id',$product->product_id],['unit','Weight']])
                    ->whereHas('Bill',function($value) use($request){
                    $value->whereDate('bill_date','<',$request->from_date);
                    })->sum('net_weight');

                    $opening_weight_indent = IndentProduct::
                    where([['product_id',$product->product_id],['unit','Weight']])
                    ->whereHas('Indent',function($value) use($request){
                    $value->whereDate('indent_date','<',$request->from_date);
                    })->sum('net_weight');

                    $indent_weight_stock =  IndentProduct::
                    where([['product_id',$product->product_id],['unit','Weight']])
                    ->whereHas('Indent',function($value) use($from_date,$to_date){
                    $value->whereBetween('indent_date',[$from_date,$to_date]);
                    })->sum('net_weight');
                        
                    $purchase_weight_stock = Billproduct::
                    where([['product_id',$product->product_id],['unit','Weight']])
                    ->whereHas('Bill',function($value) use($from_date,$to_date){
                    $value->whereBetween('bill_date',[$from_date,$to_date]);
                    })->sum('net_weight');

                    $data['weight_opening_stock']  = $product->opening_stock+$opening_weight_purchase-$opening_weight_indent;
                    $data['pieces_opening_stock']  =  0;
                    $data['weight_purchase_stock'] = $purchase_weight_stock;
                    $data['weight_indent_stock']   = $indent_weight_stock;
                    $data['weight_clossing_stock'] = ($purchase_weight_stock + $data['weight_opening_stock']) -
                                                        $data['weight_indent_stock'];
            }
            array_push($stock_products, $data);
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.stock', compact('stock_products','org','request','category','subcategory'))->stream('stock-report.pdf');
                break;
            case 'excel':
                return view('reports.stock', compact('stock_products','org','request','category','subcategory'));
                break;
            default:
                return view('reports.stock', compact('stock_products','org','request','category','subcategory'));
                break;
        }
    }

    public function reorder_level_report(Request $request)
    {
        $today            = date('Y-m-d');
        $org              = Organization::first();
        $reorder_products = [];
        $category         = '';
        $subcategory      = '';
        $supplier         = '';
        $products         = Product::with('Category','Subcategory')->get();
        if($request->subcategory_id!='' && $request->category_id!=''){
            $products     = Product::where([['category_id',$request->category_id],
                                    ['subcategory_id',$request->subcategory_id]])
                                    ->with('Category','Subcategory')
                                    ->get();
            $category     = Category::where('category_id',$request->category_id)->first();
            $subcategory  = Subcategory::where('subcategory_id',$request->subcategory_id)->first();

        }elseif ($request->category_id!='') {
            $products   = Product::where('category_id',$request->category_id)->with('Category','Subcategory')->get();
            $category   = Category::where('category_id',$request->category_id)->first();

        }elseif ($request->subcategory_id!='') {
            $products       = Product::where('subcategory_id',$request->subcategory_id)->with('Category','Subategory')->get();
            $subcategory   = Subcategory::where('subcategory_id',$request->subcategory_id)->first();
        }

        if($request->supplier_id){
            $products = $products->filter(function($value,$key) use($request){
                    return ($value->supplier_id == $request->supplier_id);
            })->values();

            $supplier = Supplier::where('supplier_id',$request->supplier_id)->first();
        }
       

        foreach ($products as $product) 
        {
            $current_stock = '';
            if($product->unit=='Pieces')
            {
                $purchase_pieces_stock = Billproduct::
                            where([['product_id',$product->product_id],['unit','Pieces']])
                            ->sum('pieces');

                $indent_pieces_stock = IndentProduct::
                            where([['product_id',$product->product_id],['unit','Pieces']])
                            ->sum('quantity');
               
                $current_stock  = ($product->opening_stock + $purchase_pieces_stock)-$indent_pieces_stock;
            }
            else
            {
                $indent_weight_stock   =  IndentProduct::where([['product_id',$product->product_id],
                                        ['unit','Weight']])->sum('net_weight');
                    
                $purchase_weight_stock = Billproduct::where([['product_id',$product->product_id],
                                        ['unit','Weight']])->sum('net_weight');

                $current_stock = ($product->opening_stock+$purchase_weight_stock)-$indent_weight_stock;
            }

            if($current_stock<=$product->reorder_level)
            {
                $data = [
                'product' => $product,
                ];
                array_push($reorder_products, $data);
            }
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.reorder_level', compact('reorder_products','request','org','today','category','subcategory','supplier'))->stream('reorder_level-report.pdf');
                break;
            case 'excel':
                return view('reports.reorder_level', compact('reorder_products','request','org','today','category','subcategory','supplier'));
                break;
            default:
                return view('reports.reorder_level', compact('reorder_products','request','org','today','category','subcategory','supplier'));
                break;
        }
    }

    public function invoice_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));
        if($request->customer_id=='')
        {
            $invoices = Invoice::
            whereBetween('invoice_date', [$from_date,$to_date])
            ->with('InvoiceProducts','Customer','PaymentTerm')->get();
            $customer = '';
        }
        else
        {
            $invoices = Invoice::where('customer_id',$request->customer_id)->
            whereBetween('invoice_date', [$from_date,$to_date])
            ->with('InvoiceProducts','Customer','PaymentTerm')->get();
            $customer = Customer::where('customer_id',$request->customer_id)->first();
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.invoice', compact('invoices','request','org','customer'))->stream('invoice-report.pdf');
                break;
            case 'excel':
                return view('reports.invoice', compact('invoices','request','org','customer'));
                break;
            default:
                return view('reports.invoice', compact('invoices','request','org','customer'));
                break;
        }
    }

     public function bill_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));
        if($request->supplier_id=='')
        {
            $bills = Bill::
            whereBetween('bill_date', [$from_date,$to_date])
            ->with('BillProducts','Supplier','PaymentTerm')->get();
            $supplier = '';
        }
        else
        {
            $bills = Bill::where('supplier_id',$request->supplier_id)->
            whereBetween('bill_date', [$from_date,$to_date])
            ->with('BillProducts','Supplier','PaymentTerm')->get();
            $supplier = Supplier::where('supplier_id',$request->supplier_id)->first();
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.bill', compact('bills','request','org','supplier'))->stream('bill-report.pdf');
                break;
            case 'excel':
                return view('reports.bill', compact('bills','request','org','supplier'));
                break;
            default:
                return view('reports.bill', compact('bills','request','org','supplier'));
                break;
        }
    }

    public function bill_tax_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));
        
        $bills = Bill::
            whereBetween('bill_date', [$from_date,$to_date])->with('Supplier')->get();

        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.bill_gst', compact('bills','request','org'))->stream('bill-tax-report.pdf');
                break;
            case 'excel':
                return view('reports.bill_gst', compact('bills','request','org'));
                break;
            default:
                return view('reports.bill_gst', compact('bills','request','org'));
                break;
        }
    }

    public function invoice_tax_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));
        
        $invoices = Invoice::
            whereBetween('invoice_date', [$from_date,$to_date])->with('Customer')->get();

        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.invoice_gst', compact('invoices','request','org'))->stream('invoice-tax-report.pdf');
                break;
            case 'excel':
                return view('reports.invoice_gst', compact('invoices','request','org'));
                break;
            default:
                return view('reports.invoice_gst', compact('invoices','request','org'));
                break;
        }
    }

    public function ledger_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));

        if($request->ledger_type=='Vendor')
        {
            $bills = Bill::whereBetween('bill_date',array($from_date,$to_date))
            ->with(['Supplier','BillProducts'])->orderBy('bill_date','desc')->get();

            $payments = Payment::whereBetween('voucher_date',array($from_date,$to_date))
            ->with(['Supplier','PaymentParticulars'])->orderBy('voucher_date','desc')->get();

            //opening amounts vendor
            $openingPaymentTotal = Bill::whereDate('bill_date','<',$request->from_date)->sum('grand_total');
            $openingReceiptTotal = Payment::whereDate('voucher_date','<',$request->from_date)->sum('total_amount');

            $supplier  =  Supplier::where('supplier_id',$request->supplier_id)->first();
            
            if($request->supplier_id!=null) {
                 //report filter
                $bills = $bills->filter(function($value,$key) use($request){
                    return ($value->supplier_id == $request->supplier_id);
                })->values();

                $payments = $payments->filter(function($value,$key) use($request){
                    return ($value->supplier_id == $request->supplier_id);
                })->values();

                //opening amount filter vendor
                $openingPaymentTotal = Bill::where('supplier_id',$request->supplier_id)
                ->whereDate('bill_date','<',$request->from_date)->sum('grand_total');

                $openingReceiptTotal = Payment::where('supplier_id',$request->supplier_id)
                ->whereDate('voucher_date','<',$request->from_date)->sum('total_amount');
            }

            switch ($request->display_type) 
            {
                case 'pdf':
                    return PDF::loadView('reports.vendor_ledger', compact('bills','payments','request','org','openingReceiptTotal','openingPaymentTotal','supplier'))->stream('vendor-report.pdf');
                    break;
                case 'excel':
                    return view('reports.vendor_ledger', compact('bills','payments','request','org','openingReceiptTotal','openingPaymentTotal','supplier'));
                    break;
                default:
                    return view('reports.vendor_ledger', compact('bills','payments','request','org','openingReceiptTotal','openingPaymentTotal','supplier'));
                    break;
            }
        }else{

             $invoices = Invoice::whereBetween('invoice_date',array($from_date,$to_date))
            ->with(['Customer','InvoiceProducts'])->orderBy('invoice_date','desc')->get();

            $receipts = Receipt::whereBetween('receipt_date',array($from_date,$to_date))
            ->with(['Customer','ReceiptParticulars'])->orderBy('receipt_date','desc')->get();

            //opening amounts vendor
            $openingPaymentTotal = Invoice::whereDate('invoice_date','<',$request->from_date)->sum('grand_total');
            $openingReceiptTotal = Receipt::whereDate('receipt_date','<',$request->from_date)->sum('total_amount');

            $customer  =  Customer::where('customer_id',$request->customer_id)->first();
                                        // dd($receipts);

            if($request->customer_id!=null) 
            {
                 //report filter
                $invoices = $invoices->filter(function($value,$key) use($request){
                    return ($value->customer_id == $request->customer_id);
                })->values();

                $receipts = $receipts->filter(function($value,$key) use($request){
                    return ($value->customer_id == $request->customer_id);
                })->values();

                //opening amount filter vendor
                $openingPaymentTotal = Invoice::where('customer_id',$request->customer_id)
                ->whereDate('invoice_date','<',$request->from_date)->sum('grand_total');

                $openingReceiptTotal = Receipt::where('customer_id',$request->customer_id)
                ->whereDate('receipt_date','<',$request->from_date)->sum('total_amount');

            }
            switch ($request->display_type) 
            {
                case 'pdf':
                    return PDF::loadView('reports.customer_ledger', compact('invoices','receipts','request','org','openingReceiptTotal','openingPaymentTotal','customer'))->stream('vendor-report.pdf');
                    break;
                case 'excel':
                    return view('reports.customer_ledger', compact('invoices','receipts','request','org','openingReceiptTotal','openingPaymentTotal','customer'));
                    break;
                default:
                    return view('reports.customer_ledger', compact('invoices','receipts','request','org','openingReceiptTotal','openingPaymentTotal','customer'));
                    break;
            }
        }
    }

     public function quotation_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));
        if($request->customer_id=='')
        {
            $quotations = Quotation::
            whereBetween('quotation_date', [$from_date,$to_date])
            ->with('QuotationProducts','Customer')->get();
            $customer = '';
        }
        else
        {
            $quotations = Quotation::where('customer_id',$request->customer_id)->
            whereBetween('quotation_date', [$from_date,$to_date])
            ->with('QuotationProducts','Customer')->get();
            $customer = Customer::where('customer_id',$request->customer_id)->first();
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.quotation', compact('quotations','request','org','customer'))->stream('quotation-report.pdf');
                break;
            case 'excel':
                return view('reports.quotation', compact('quotations','request','org','customer'));
                break;
            default:
                return view('reports.quotation', compact('quotations','request','org','customer'));
                break;
        }
    }

    public function proforma_report(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));
        if($request->customer_id=='')
        {
            $proformas = Proforma::
            whereBetween('proforma_date', [$from_date,$to_date])
            ->with('ProformaProducts','Customer')->get();
            $customer = '';
        }
        else
        {
            $proformas = Proforma::where('customer_id',$request->customer_id)->
            whereBetween('proforma_date', [$from_date,$to_date])
            ->with('ProformaProducts','Customer')->get();
            $customer = Customer::where('customer_id',$request->customer_id)->first();
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.proforma', compact('proformas','request','org','customer'))->stream('proforma-report.pdf');
                break;
            case 'excel':
                return view('reports.proforma', compact('proformas','request','org','customer'));
                break;
            default:
                return view('reports.proforma', compact('proformas','request','org','customer'));
                break;
        }
    }

    public function indent_report(Request $request)
    {
        $customer       = '';
        $employee       = '';
        $org            = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));

        $indents = Indent::whereBetween('indent_date', [$from_date,$to_date])
            ->with('IndentProducts','Customer','Employee')->get();

        if($request->customer_id!='')
        {
            $customer = Customer::where('customer_id',$request->customer_id)->first();
            $indents = $indents->filter(function($value,$key) use($request){
                    return ($value->customer_id == $request->customer_id);
                })->values();
        }
        if($request->employee_id!='')
        {
            $employee = Employee::where('employee_id',$request->employee_id)->first();
            $indents = $indents->filter(function($value,$key) use($request){
                    return ($value->employee_id == $request->employee_id);
                })->values();
        }
        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.indent', compact('indents','request','org','customer','employee'))->stream('indent-report.pdf');
                break;
            case 'excel':
                return view('reports.indent', compact('indents','request','org','customer','employee'));
                break;
            default:
                return view('reports.indent', compact('indents','request','org','customer','employee'));
                break;
        }
    }

    public function payment_receipt(Request $request)
    {
        $org = Organization::first();   
        $to_date        = date('Y-m-d',strtotime($request->to_date));
        $from_date      = date('Y-m-d',strtotime($request->from_date));

        $expenses = Payment::whereBetween('voucher_date',array($from_date,$to_date))
            ->with(['Supplier','PaymentParticulars'])->orderBy('voucher_date','desc')->get();
      
        $incomes = Receipt::whereBetween('receipt_date',array($from_date,$to_date))
        ->with(['Customer','ReceiptParticulars'])->orderBy('receipt_date','desc')->get();

        $openingPaymentTotal = Payment::whereDate('voucher_date','<',$request->from_date)->sum('total_amount');
        $openingReceiptTotal = Receipt::whereDate('receipt_date','<',$request->from_date)->sum('total_amount');

        switch ($request->display_type) 
        {
            case 'pdf':
                return PDF::loadView('reports.payment_receipt', compact('incomes','expenses','request','org','openingReceiptTotal','openingPaymentTotal'))->stream('vendor-report.pdf');
                break;
            case 'excel':
                return view('reports.payment_receipt', compact('incomes','request','expenses','org','openingReceiptTotal','openingPaymentTotal'));
                break;
            default:
                return view('reports.payment_receipt', compact('incomes','request','expenses','org','openingReceiptTotal','openingPaymentTotal'));
                break;
        }
    }

}




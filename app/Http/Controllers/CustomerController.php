<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Module\Module;
use PDF;
use Mail;
use Auth;
use App\Customer;
use App\CustomerDocument;
use App\Imports\CustomerImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\CustomerMail; 

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return Customer::where('customer_name', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->orWhere('city', 'like', '%'.$request->search.'%')
            ->orWhere('address', 'like', '%'.$request->search.'%')
            ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
            ->paginate(10); 
    }   

    public function store(Request $request)
    {

        $data = $this->validate($request, [
            'customer_name'         => 'required|max:255',
            'email'                 => 'sometimes|email|max:50|unique:customers,email,{$customer_id},email',
            'mobile_no'             => 'sometimes|nullable|digits:10|regex:/[0-9]{10}/',
            'gstin_no'              => 'sometimes|nullable|max:15|min:15',
            'address'               => 'sometimes|nullable|max:255',
            'city'                  => 'sometimes',
            'pincode'               => 'sometimes|nullable|regex:/[0-9]{6}/',
        ]);
        
        $customer = Customer::create($data);
    }

    public function show($customer_id)
    {
       return Customer::where('customer_id',$customer_id)->first();
    } 

    public function update(Request $request,$customer_id)
    {
        $data = $this->validate($request, [
          
            'customer_name'         => 'required|max:255',
            'email'                 => 'sometimes|email|max:50|unique:customers,email,'.$request->customer_id.',customer_id',
            'mobile_no'             => 'sometimes|nullable|digits:10|regex:/[0-9]{10}/',
            'gstin_no'              => 'sometimes|nullable|max:15|min:15',
            'address'               => 'sometimes|nullable|max:255',
            'city'                  => 'sometimes',
            'pincode'               => 'sometimes|nullable|regex:/[0-9]{6}/',
        ]);
        Customer::where('customer_id',$customer_id)->update($data);
       
    }

    public function destroy($customer_id)
    {
        
        $customer  = Customer::where('customer_id',$customer_id)->delete();

        if ($customer) {
            return $customer_id;
        }
        
    }

    public function get_customers()
    {
        return Customer::get();
    }

    public function customer_active(Request $request)
    {
        $cust_active = Customer::where('customer_id',$request->customer_id)->update([
            'status' => 1,
        ]);

        return $cust_active;
    }

    public function get_limit_customers(Request $request)
    {
        return Customer::where('status',1)
            ->where(function($query) use($request){
                $query->where('customer_name','like',"%$request->search%")
                ->orWhere('customer_code', 'like', "%$request->search%")
                ->orWhere('mobile_no', 'like', "%$request->search%");
            })
            ->limit(10)
            ->orderBy('customer_name')
            ->with('ReceiptAdvances')
            ->get();
    }
}

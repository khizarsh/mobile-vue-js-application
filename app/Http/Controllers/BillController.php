<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use App\Module\Module;

use App\Preference;
use App\Organization;
use App\Bill;
use App\BillProduct;
use App\Supplier;
use App\Product;
use App\PaymentTerm;
use App\Term;
use App\Tax;
use App\Customer;

use App\PaymentParticular;
use App\Payment;
use App\Bank;
use App\Master;

use App\PaymentAdvance;


class BillController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return Bill::whereHas('Supplier', function($query) use($request){
                $query->where('supplier_name','like', "%$request->search%");
            })
            ->orWhere('bill_no', 'like', '%'.$request->search.'%')
            ->orWhere('bill_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Supplier')
            ->orderBy('bill_id','DESC')
            ->paginate(10);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'bill_no'            => 'required|max:50|unique:bills,bill_no,{$bill_id},bill_no,deleted_at,NULL',
            'bill_date'          => 'required|date',
            'reference_no'       => 'max:50',
            'reference_date'     => 'sometimes|nullable|date',
            'supplier_id'        => 'required|numeric',           
            'source_id'          => 'required|numeric',
            'destination_id'     => 'required|numeric',
            'payment_term_id'    => 'required|numeric',
            'due_date'           => 'required|date',
            'sub_total'          => 'required|numeric|not_in:0',
            'discount_type'      => 'required|max:50',
            'discount'           => 'required|numeric|min:0',
            'discount_amount'    => 'required|numeric|min:0',
            'shipping_charges'   => 'required|numeric|min:0',
            'hallmark_charges'   => 'required|numeric|min:0',
            'packing_charges'    => 'required|numeric|min:0',
            'tax_id'             => 'required|numeric',
            'tax_amount'         => 'required|numeric',
            'total_amount'       => 'required|numeric',
            'round_off'          => 'required|numeric',
            'grand_total'        => 'required|numeric',
            'term_id'            => 'sometimes|nullable|numeric',
            'terms'              => 'nullable|max:255',
            'note'               => 'nullable|max:255',
        ]);

        $preference = Preference::first();
        $supplier    = Supplier::where('supplier_id',$request->supplier_id)->first();

        $data['fiscal_year']      = $preference->fiscal_year;
        $data['billing_address']  = $supplier->billing_address;
        $data['shipping_address'] = $supplier->shipping_address;
        $data['reference_date']   = $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : '';

        $collect = collect($request->bill_products);

        
        $sub_total_check   = $collect->reduce(function ($carry, $item) {
                          return $carry + $item['sub_total'];
                      });

        if ($sub_total_check > 0 ) {

            if(strpos($request->document,'base64') !== false) 
            {
                //To get Base64 extension
                $uri = $request->document;
                $img = explode(',', $uri);
                $ini =substr($img[0], 11);
                $type = explode(';', $ini); 

                $image_parts            = explode(";base64,", $request->document);
                $image_base64           = base64_decode($image_parts[1]);

                if($type[0]!='png' && $type[0]!='jpeg')
                {
                    $image_type_aux     = explode("application/", $image_parts[0]);
                }else{
                    $image_type_aux     = explode("image/", $image_parts[0]);
                }
                $png_url                = date('Ymdhis').".".$image_type_aux[1];
                $path                   = public_path().'/storage/bills/' .$png_url;
                file_put_contents($path, $image_base64);
                $data['document']    = $png_url;
            }

            $bill = Bill::create($data);
            
            foreach ($request->bill_products as $bill_product) 
            {
                $product = Product::where('product_id',$bill_product['product_id'])->first();
                $bill_prod_up = BillProduct::create([
                    'bill_id'            => $bill['bill_id'],
                    'classification_id'  => $bill_product['classification_id'],
                    'category_id'        => $bill_product['category_id'],
                    'subcategory_id'     => $bill_product['subcategory_id'],
                    'product_id'         => $bill_product['product_id'],
                    'product_type'       => $bill_product['product_type'],
                    'product_code'       => $bill_product['product_code'],
                    'hsn_code'           => $product->hsn_code,
                    'product_name'       => $bill_product['product_name'],
                    'description'        => $product->description,
                    'size'               => $product->size,
                    'stone_color'        => $product->stone_color,
                    'purity'             => $product->purity,
                    'special_product'    => $product->special_product,
                    'unit'               => $bill_product['unit'],
                    'gross_weight'       => $bill_product['gross_weight'],
                    'cover_weight'       => $product->cover_weight,
                    'net_weight'         => $bill_product['net_weight'],
                    'pieces'             => $bill_product['pieces'],
                    'silver_rate'        => $bill_product['silver_rate'],
                    'product_price_id'   => $bill_product['product_price_id'], 
                    'customer_type'      => $bill_product['customer_type'], 
                    'making_charges'     => $bill_product['making_charges'],
                    'net_rate'           => $bill_product['net_rate'],
                    'piece_rate'         => $bill_product['piece_rate'],
                    'amount'             => $bill_product['amount'],
                    'other_charges'      => $bill_product['other_charges'],
                    'box_code'           => $bill_product['box_code'],
                    'box_rate'           => $bill_product['box_rate'],
                    'box_amount'         => $bill_product['box_amount'],
                    'sub_total'          => $bill_product['sub_total'],
                ]);

                if(!empty($request->reference)){
                    BillProduct::where('bill_product_id',$bill_prod_up['bill_product_id'])->update([
                        'reference' => 'App'."\\".$request->reference,
                        'reference_id' => $bill_product[$request->reference_id],
                    ]);
                    //update quotation product status
                }
            }

            if( $request->due_date == $request->bill_date ){
                $diffrence = 0;
                $this->Payment($request,$supplier,$bill,$diffrence);

            }else{
                $payment_advance = PaymentAdvance::where('supplier_id',$request->supplier_id)->get();

                $credit =  $payment_advance->filter(function($query){
                                return $query->transaction_type == 'credit';
                            })->sum('amount');

                $debit =  $payment_advance->filter(function($query){
                                return $query->transaction_type == 'debit';
                            })->sum('amount');

                $diffrence = $credit-$debit;

                if ($diffrence > 0) {
                    $this->Payment($request,$supplier,$bill,$diffrence);
                }
            }

        }else{
            $data = $this->validate($request, [
                'bill_products'  => 'required',
                'classification_id' => 'required',
                'category_id'        => 'required',
                'subcategory_id'    => 'required',
                'product_id'        => 'required',
            ],[
                'classification_id.required' => 'Add Bill Products',
                'category_id.required' => 'Add Bill Products',
                'subcategory_id.required' => 'Add Bill Products',
                'product_id.required' => 'Add Bill Products',
            ]);
        }
    }

    public function Payment($request,$supplier,$bill,$diffrence)
    {
        $preference = Preference::first();

        $module = new Module();               
        $voucher_no = $module->auto_generate_no('Payment','payment_id');

        $payment_mode = Master::where('master_name','Payment Mode')->first();

        $bank = Bank::first();

        $payment = Payment::create([
                    'fiscal_year'       => $preference->fiscal_year,
                    'voucher_no'        => $voucher_no,
                    'voucher_date'      => date("Y-m-d", strtotime($request->bill_date)),
                    'reference_no'      => $request->reference_no,
                    'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
                    'supplier_id'       => $request->supplier_id,
                    'billing_address'   => $supplier->billing_address,
                    'shipping_address'  => $supplier->shipping_address,
                    'bank_id'           => $bank['bank_id'],
                    'payment_mode'      => $payment_mode['master_value'],
                    'total_amount'      => 0,
                    'term_id'           => $request->term_id,
                    'terms'             => $request->terms,
                    'note'              => $request->note,
                ]);

        if ($diffrence == 0 || ($diffrence > $request->grand_total || $diffrence == $request->grand_total)) {
            $paid_amount = $request->grand_total;
        }else{
            $paid_amount = $diffrence;
        }

        $payment_part = PaymentParticular::create([
            'payment_id'    => $payment->payment_id,
            'reference'     => 'App\Bill',
            'reference_id'  => $bill['bill_id'],
            'paid_amount'   => $paid_amount,
        ]);

        //total_amount update
        Payment::where('payment_id',$payment->payment_id)->update([
            'total_amount' => $paid_amount,
        ]);

        if ($diffrence !=0) {
            PaymentAdvance::create([
                'bill_id'          => $bill['bill_id'],
                'payment_id'       => $payment->payment_id,
                'supplier_id'      => $request->supplier_id,
                'payment_mode'     => $payment->payment_mode,
                'bank_id'          => $payment->bank_id,
                'transaction_type' => 'debit',
                'amount'           => $paid_amount,
                'advance_date'     => date('Y-m-d',strtotime($payment->voucher_date)),
            ]);
        }

        return $payment;
    }
    
    public function show($bill_id)
    {
        return Bill::where('bill_id',$bill_id)->with('BillProducts','Supplier','PaymentTerm','PaymentParticular')->first();
    }
   
    public function update(Request $request, $bill_id)
    {
           $data = $this->validate($request, [
            'bill_no'            => 'required|max:50|unique:bills,bill_no,'.$request->bill_id.',bill_id,deleted_at,NULL',
            'bill_date'          => 'required|date',
            'reference_no'       => 'max:50',
            'reference_date'     => 'sometimes|nullable|date',
            'supplier_id'        => 'required|numeric',           
            'source_id'          => 'required|numeric',
            'destination_id'     => 'required|numeric',
            'payment_term_id'    => 'required|numeric',
            'due_date'           => 'required|date',
            'sub_total'          => 'required|numeric',
            'discount_type'      => 'required|max:50',
            'discount'           => 'required|numeric|min:0',
            'discount_amount'    => 'required|numeric|min:0',
            'shipping_charges'   => 'required|numeric|min:0',
            'hallmark_charges'   => 'required|numeric|min:0',
            'packing_charges'    => 'required|numeric|min:0',
            'tax_id'             => 'required|numeric',
            'tax_amount'         => 'required|numeric',
            'total_amount'       => 'required|numeric',
            'round_off'          => 'required|numeric',
            'grand_total'        => 'required|numeric',
            'term_id'            => 'sometimes|nullable|numeric',
            'terms'              => 'max:255',
            'note'               => 'max:255',
        ]);

        $preference         = Preference::first();
        $suplier            = Supplier::where('supplier_id',$request->supplier_id)->first();
        $bill               = Bill::where('bill_id',$request->bill_id)->first();
        $bill_products      = Bill::where('bill_id',$request->bill_id)->get();

        $data['reference_date'] = $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $bill->reference_date;

        $collect = collect($request->bill_products);

        
        $net_weight_check   = $collect->reduce(function ($carry, $item) {
                          return $carry + $item['net_weight'];
                      });

        if ($net_weight_check > 0) {

            if(strpos($request->document,'base64') !== false) 
            {
                //To get Base64 extension
                $uri = $request->document;
                $img = explode(',', $uri);
                $ini =substr($img[0], 11);
                $type = explode(';', $ini); 

                $image_parts            = explode(";base64,", $request->document);
                $image_base64           = base64_decode($image_parts[1]);

                if($type[0]!='png' && $type[0]!='jpeg')
                {
                    $image_type_aux     = explode("application/", $image_parts[0]);
                }else{
                    $image_type_aux     = explode("image/", $image_parts[0]);
                }
                $png_url                = date('Ymdhis').".".$image_type_aux[1];
                $path                   = public_path().'/storage/bills/' .$png_url;
                file_put_contents($path, $image_base64);
                $data['document']    = $png_url;
            }
        
            Bill::where('bill_id',$bill_id)->update($data);

            //Selected bill Products Delete
            foreach ($request->deleted_bill_products as $bill_product) 
            {
                BillProduct::where('bill_product_id',$bill_product['bill_product_id'])->delete();
            }

            foreach ($request->bill_products as $bill_product) 
            {
                $product = Product::where('product_id',$bill_product['product_id'])->first();

                $data = [
                    'bill_id'         => $bill['bill_id'],
                    'classification_id'  => $bill_product['classification_id'],
                    'category_id'        => $bill_product['category_id'],
                    'subcategory_id'     => $bill_product['subcategory_id'],
                    'product_id'         => $bill_product['product_id'],
                    'product_type'       => $bill_product['product_type'],
                    'product_code'       => $bill_product['product_code'],
                    'hsn_code'           => $product->hsn_code,
                    'product_name'       => $bill_product['product_name'],
                    'description'        => $product->description,
                    'size'               => $product->size,
                    'stone_color'        => $product->stone_color,
                    'purity'             => $product->purity,
                    'special_product'    => $product->special_product,
                    'unit'               => $bill_product['unit'],
                    'gross_weight'       => $bill_product['gross_weight'],
                    'cover_weight'       => $product->cover_weight,
                    'net_weight'         => $bill_product['net_weight'],
                    'pieces'             => $bill_product['pieces'],
                    'silver_rate'        => $bill_product['silver_rate'],
                    'product_price_id'   => $bill_product['product_price_id'], 
                    'customer_type'      => $bill_product['customer_type'], 
                    'making_charges'     => $bill_product['making_charges'],
                    'net_rate'           => $bill_product['net_rate'],
                    'piece_rate'         => $bill_product['piece_rate'],
                    'amount'             => $bill_product['amount'],
                    'other_charges'      => $bill_product['other_charges'],
                    'box_code'           => $bill_product['box_code'],
                    'box_rate'           => $bill_product['box_rate'],
                    'box_amount'         => $bill_product['box_amount'],
                    'sub_total'          => $bill_product['sub_total'],
                ];

                if(empty($bill_product['bill_product_id'])) {
                    $bill_prod_up = BillProduct::create($data);
                }else{
                    $bill_prod_up = BillProduct::where('bill_product_id',$bill_product['bill_product_id'])->update($data);
                }
            }
            
        }else{
            $data = $this->validate($request, [
                'bill_products'  => 'required',
                'classification_id'  => 'required',
                'category_id'       => 'required',
                'subcategory_id'    => 'required',
                'product_id'        => 'required',
            ],[
                'classification_id.required' => 'Add Bill Products Or delete Bill',
                'category_id.required' => 'Add Bill Products Or delete Bill',
                'subcategory_id.required' => 'Add Bill Products Or delete Bill',
                'product_id.required' => 'Add Bill Products Or delete Bill',
            ]);
        }

    }
   
    public function destroy($bill_id)
    {
        BillProduct::where('bill_id',$bill_id)->delete();
        Bill::where('bill_id',$bill_id)->delete();
    }

    public function preview($bill_id)
    {
        $org = Organization::first();
        $bill = Bill::where('bill_id',$bill_id)->with('Supplier','BillProducts')->first();

        PDF::loadView('purchase.bill.pdf1', compact('bill','org'), [], [
            'margin_top' => 41.8,
        ])->stream($bill->bill_no.'.pdf');
    }

    public function get_unpaid_bills(Request $request)
    {
        $bills =  Bill::where('supplier_id',$request->supplier_id)->get();
        $result = [];
        foreach ($bills as $bill) 
        {
            $paid_amount = PaymentParticular::where('reference_id',$bill->bill_id)->sum('paid_amount');

            $due_amount = $bill->grand_total - $paid_amount;

            if($due_amount!=0)
            {
                $data = [
                    'check'          => false,
                    'bill_id'        => $bill->bill_id,
                    'bill_no'        => $bill->bill_no,
                    'bill_date'      => $bill->bill_date,
                    'grand_total'    => $bill->grand_total,
                    'paid_amount'    => $paid_amount,
                    'due_amount'     => $due_amount,
                    'payable_amount' => 0.00,
                    //'payable_amount' => $due_amount,
                ];
                array_push($result, $data);
            }
        }
        return $result;
    }

}

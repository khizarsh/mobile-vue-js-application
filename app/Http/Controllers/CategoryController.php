<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Category;
use App\Subcategory;

class CategoryController extends Controller
{
    // 57,55,53,49,49,53
    //         ns-49,52,
    //         m - 50,
    //         ss-50
    //         p-50
    //         b-46
    //         keshav-54,51
    //         banu-42
    //         man-48
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return Category::
            where('category_name', 'like', '%'.$request->search.'%')
            ->orWhere('description', 'like', '%'.$request->search.'%')
            ->paginate(5);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'category_name'  => 'required|unique:categories,category_name',
            'description'       => 'nullable|max:255',
        ],[
            'category_name.unique' => 'The subcategory name has already been taken for selected category'
        ]);

        $category = Category::create($data);
    }
    
    public function show($category_id)
    {
        return Category::where('category_id',$category_id)->first();
    }
   
    public function update(Request $request, $category_id)
    {
        $data = $this->validate($request, [
            'category_id'       => 'required',
            'category_name'  => 'required|max:50|unique:categories,category_name,'.$request->category_id.',category_id',
            'description'       => 'nullable|max:255',
        ],[
            'category_name.unique' => 'The subcategory name has already been taken for selected category'
        ]);
        Category::where('category_id',$category_id)->update($data);
    }
   
    public function destroy($category_id)
    {
        Category::where('category_id',$category_id)->delete();
    }

    public function get_categories()
    {
        return Category::get();
    
    }

    public function get_limit_categories(Request $request)
    {
        return Category::where('category_name','like',"%$request->search%")
                    ->limit(10)
                    ->get();
    }
}

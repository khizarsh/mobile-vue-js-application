<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Supplier;
use App\PaymentTerm;
use App\Place;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return Supplier::where('supplier_name', 'like', '%'.$request->search.'%')
            ->orWhere('supplier_code', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
            ->paginate(10); 
    }   

    public function store(Request $request)
    {

        $data = $this->validate($request, [
            'supplier_code'         => 'required|max:50|unique:suppliers,supplier_code',
            'supplier_name'         => 'required|max:255',
            'email'                 => 'required|email|max:50|unique:suppliers,email',
            'mobile_no'             => 'required|digits:10|regex:/[0-9]{10}/|unique:suppliers,mobile_no',
            'phone_no'              => 'digits:11|sometimes|nullable|regex:/[0-9]{10}/',
            'gstin_no'              => 'sometimes|nullable|max:15|min:15',
        ]);
        
        return $supplier = Supplier::create($data);
    }

    public function show($supplier_id)
    {
       return Supplier::where('supplier_id',$supplier_id)->first();
    } 

    public function update(Request $request,$supplier_id)
    {
        $data = $this->validate($request, [
            'supplier_code'         => 'required|max:50|unique:suppliers,supplier_code,'.$request->supplier_id.',supplier_id',
            'supplier_name'         => 'required|max:255',
            'email'                 => 'required|email|max:50|unique:suppliers,email,'.$request->supplier_id.',supplier_id',
            'mobile_no'             => 'required|digits:10|regex:/[0-9]{10}/|unique:suppliers,mobile_no,'.$request->supplier_id.',supplier_id',
            'phone_no'              => 'digits:11|sometimes|nullable|regex:/[0-9]{10}/',
            'gstin_no'              => 'sometimes|nullable|max:15|min:15',
        ]);

        Supplier::where('supplier_id',$supplier_id)->update($data);
       
    }

    public function destroy($supplier_id)
    {
        $supplier  = Supplier::where('supplier_id',$supplier_id)->delete();

        if ($supplier) {
            return $supplier_id;
        }
        
    }

    public function get_suppliers()
    {
        return Supplier::get();
    }

    public function get_supplier_code()
    {
        $id = Supplier::max('supplier_id');
        $supplier_code = $id + 1;
        $length = strlen($supplier_code);
        $n = 4 - $length;
        for ($i=0; $i < $n; $i++) 
        { 
            $supplier_code = '0'.$supplier_code;
        }
        return $supplier_code;
    }

    public function get_limit_suppliers(Request $request)
    {
        return Supplier::where('supplier_name','like',"%$request->search%")
            ->orWhere('supplier_code', 'like', "%$request->search%")
            ->orWhere('mobile_no', 'like', "%$request->search%")
            ->limit(10)
            ->orderBy('supplier_name')
            ->get();
    }
}

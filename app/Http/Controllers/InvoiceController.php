<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use Mail;
use App\Module\Module;

use App\IndentProduct;
use App\QuotationProduct;
use App\Preference;
use App\Organization;
use App\Indent;
use App\Invoice;
use App\InvoiceProduct;
use App\Customer;
use App\Employee;
use App\Product;
use App\PaymentTerm;
use App\Term;
use App\Tax;
use App\ProformaProduct;

use App\Receipt;
use App\ReceiptParticular;
use App\Bank;
use App\Master;

use App\ReceiptAdvance;

use App\Mail\NewInvoice; 
use App\Mail\NewReceipt; 

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return Invoice::whereHas('Customer', function($query) use($request){
                $query->where('customer_name','like', "%$request->search%");
            })
            ->orWhereHas('Employee', function($query) use($request){
                $query->where('employee_name','like', "%$request->search%");
            })
            ->orWhere('invoice_no', 'like', '%'.$request->search.'%')
            ->orWhere('invoice_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Customer','Employee')
            ->orderBy('invoice_id','DESC')
            ->paginate(10);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'invoice_no'            => 'required|max:50|unique:invoices,invoice_no,{$invoice_id},invoice_no,deleted_at,NULL',
            'invoice_date'       => 'required|date',
            'reference_no'       => 'max:50',
            'reference_date'     => 'sometimes|nullable|date',
            'employee_id'        => 'required|numeric',           
            'customer_id'        => 'required|numeric',           
            'source_id'          => 'required|numeric',
            'destination_id'     => 'required|numeric',
            'payment_term_id'    => 'required|numeric',
            'due_date'           => 'required|date',
            'sub_total'          => 'required|numeric',
            'discount_type'      => 'required|max:50',
            'discount'           => 'required|numeric|min:0',
            'discount_amount'    => 'required|numeric|min:0',
            'shipping_charges'   => 'required|numeric|min:0',
            'hallmark_charges'   => 'required|numeric|min:0',
            'packing_charges'    => 'required|numeric|min:0',
            'tax_id'             => 'required|numeric',
            'tax_amount'         => 'required|numeric',
            'total_amount'       => 'required|numeric',
            'round_off'          => 'required|numeric',
            'grand_total'        => 'required|numeric',
            'term_id'            => 'sometimes|nullable|numeric',
            'terms'              => 'nullable|max:255',
            'note'               => 'nullable|max:255',
        ]);

        $preference = Preference::first();
        $customer   = Customer::where('customer_id',$request->customer_id)->first();

        $data['fiscal_year']      = $preference->fiscal_year;
        $data['billing_address']  = $customer->billing_address;
        $data['shipping_address'] = $customer->shipping_address;
        $data['reference_date']   = $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : '';

        $collect = collect($request->invoice_products);

        
        $sub_total_check   = $collect->reduce(function ($carry, $item) {
                          return $carry + $item['sub_total'];
                      });

        if ($sub_total_check > 0 ) {

            $invoice = Invoice::create($data);
            
            foreach ($request->invoice_products as $invoice_product) 
            {
                $product = Product::where('product_id',$invoice_product['product_id'])->first();
                
                $invoice_prod_up = InvoiceProduct::create([
                    'invoice_id'         => $invoice['invoice_id'],
                    'classification_id'  => $invoice_product['classification_id'],
                    'category_id'        => $invoice_product['category_id'],
                    'subcategory_id'     => $invoice_product['subcategory_id'],
                    'product_id'         => $invoice_product['product_id'],
                    'product_type'       => $invoice_product['product_type'],
                    'product_code'       => $invoice_product['product_code'],
                    'hsn_code'           => $product->hsn_code,
                    'product_name'       => $invoice_product['product_name'],
                    'description'        => $product->description,
                    'size'               => $product->size,
                    'stone_color'        => $product->stone_color,
                    'purity'             => $product->purity,
                    'special_product'    => $product->special_product,
                    'unit'               => $invoice_product['unit'],
                    'gross_weight'       => $invoice_product['gross_weight'],
                    'cover_weight'       => $product['cover_weight'],
                    'net_weight'         => $invoice_product['net_weight'],
                    'pieces'             => $invoice_product['pieces'],
                    'silver_rate'        => $invoice_product['silver_rate'],
                    'product_price_id'   => $invoice_product['product_price_id'], 
                    'customer_type'      => $invoice_product['customer_type'], 
                    'making_charges'     => $invoice_product['making_charges'],
                    'net_rate'           => $invoice_product['net_rate'],
                    'piece_rate'         => $invoice_product['piece_rate'],
                    'amount'             => $invoice_product['amount'],
                    'other_charges'      => $invoice_product['other_charges'],
                    'box_code'           => $invoice_product['box_code'],
                    'box_rate'           => $invoice_product['box_rate'],
                    'box_amount'         => $invoice_product['box_amount'],
                    'sub_total'          => $invoice_product['sub_total'],
                ]);

                if(!empty($request->reference)){
                    InvoiceProduct::where('invoice_product_id',$invoice_prod_up['invoice_product_id'])->update([
                        'reference' => 'App'."\\".$request->reference,
                        'reference_id' => $invoice_product[$request->reference_id],
                    ]);
                    //update quotation product status
                    if($request->reference=='QuotationProduct'){
                        $update_status = QuotationProduct::where('quotation_product_id',$invoice_product[$request->reference_id])->update(['invoice_status'=>1]);
                    }else if($request->reference=='IndentProduct'){
                        $update_status = IndentProduct::where('indent_product_id',$invoice_product[$request->reference_id])->update(['invoice_status'=>1]);
                    }else if($request->reference=='ProformaProduct'){
                        $update_status = ProformaProduct::where('proforma_product_id',$invoice_product[$request->reference_id])->update(['invoice_status'=>1]);
                    }
                }
            }

            if($request->due_date==$request->invoice_date){
                $diffrence = 0;
                $this->Receipt($request,$customer,$invoice,$diffrence);

            }else{
                $payment_advance = ReceiptAdvance::where('customer_id',$request->customer_id)->get();

                $credit =  $payment_advance->filter(function($query){
                                return $query->transaction_type == 'credit';
                            })->sum('amount');

                $debit =  $payment_advance->filter(function($query){
                                return $query->transaction_type == 'debit';
                            })->sum('amount');

                $diffrence = $credit-$debit;

                if ($diffrence > 0) {
                    $this->Receipt($request,$customer,$invoice,$diffrence);
                }
            }

        }else{
            $data = $this->validate($request, [
                'invoice_products'  => 'required',
                'classification_id'  => 'required',
                'category_id'        => 'required',
                'subcategory_id'    => 'required',
                'product_id'        => 'required',
            ],[
                'classification_id.required' => 'Add Invoice Products',
                'category_id.required' => 'Add Invoice Products',
                'subcategory_id.required' => 'Add Invoice Products',
                'product_id.required' => 'Add Invoice Products',
            ]);
        }

    }

    public function Receipt($request,$customer,$invoice,$diffrence)
    {
        $preference = Preference::first();

        $module = new Module();               
        $receipt_no = $module->auto_generate_no('Receipt','receipt_id');

        $payment_mode = Master::where('master_name','Payment Mode')->first();

        $bank = Bank::first();

        $receipt = Receipt::create([
                    'fiscal_year'       => $preference->fiscal_year,
                    'receipt_no'        => $receipt_no,
                    'receipt_date'      => date("Y-m-d", strtotime($request->invoice_date)),
                    'reference_no'      => $request->reference_no,
                    'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
                    'customer_id'       => $request->customer_id,
                    'billing_address'   => $customer->billing_address,
                    'shipping_address'  => $customer->shipping_address,
                    'bank_id'           => $bank['bank_id'],
                    'payment_mode'      => $payment_mode['master_value'],
                    'total_amount'      => 0,
                    'term_id'           => $request->term_id,
                    'terms'             => $request->terms,
                    'note'              => $request->note,
                ]);

        if ($diffrence == 0 || ($diffrence > $request->grand_total || $diffrence == $request->grand_total)) {
            $paid_amount = $request->grand_total;
        }else{
            $paid_amount = $diffrence;
        }

        ReceiptParticular::create([
            'receipt_id'    => $receipt->receipt_id,
            'reference'     => 'App\Invoice',
            'reference_id'  => $invoice['invoice_id'],
            'paid_amount'   => $paid_amount,
        ]);

        //total_amount update
        Receipt::where('receipt_id',$receipt->receipt_id)->update([
            'total_amount' => $paid_amount,
        ]);

        //Receipt Mail
        // $org = Organization::first();
        // Mail::send(new NewReceipt($receipt,$request,$org));

        // if ($diffrence !=0) {
        //     ReceiptAdvance::create([
        //         'invoice_id'       => $invoice['invoice_id'],
        //         'receipt_id'       => $receipt->receipt_id,
        //         'customer_id'      => $request->customer_id,
        //         'payment_mode'     => $receipt->payment_mode,
        //         'bank_id'          => $receipt->bank_id,
        //         'transaction_type' => 'debit',
        //         'amount'           => $paid_amount,
        //         'advance_date'     => date('Y-m-d',strtotime($receipt->receipt_date)),
        //     ]);
        // }

        return $receipt;
    }

    public function show($invoice_id)
    {
        return Invoice::where('invoice_id',$invoice_id)->with('InvoiceProducts','Employee','Customer','PaymentTerm','ReceiptParticular','Tax')->first();
    }
   
    public function update(Request $request, $invoice_id)
    {
        // $data = $this->validate($request, [
        //     'invoice_no'        => 'required|max:50',
        //     'invoice_date'      => 'required|date',
        //     'reference_no'      => 'max:50',
        //     'reference_date'    => 'sometimes|nullable|date',
        //     'customer_id'       => 'required|numeric',
        //     'source_id'         => 'required|numeric',
        //     'destination_id'    => 'required|numeric',
        //     'payment_term_id'   => 'required|numeric',
        //     'due_date'          => 'required|date',
        //     'sub_total'         => 'required|numeric',
        //     'discount_amount'   => 'required|numeric',
        //     'tax_amount'        => 'required|numeric',
        //     'round_off'         => 'required|numeric',
        //     'total_amount'      => 'required|numeric',
        //     'grand_total'       => 'required|numeric',
        //     'term_id'           => 'sometimes|nullable|numeric',
        //     'terms'             => 'max:2550',
        //     'note'              => 'max:2550',
        // ]);

        $data = $this->validate($request, [
             'invoice_no'            => 'required|max:50|unique:invoices,invoice_no,'.$request->invoice_id.',invoice_id,deleted_at,NULL',
            'invoice_date'       => 'required|date',
            'reference_no'       => 'max:50',
            'reference_date'     => 'sometimes|nullable|date',
            'employee_id'        => 'required|numeric',           
            'customer_id'        => 'required|numeric',           
            'source_id'          => 'required|numeric',
            'destination_id'     => 'required|numeric',
            'payment_term_id'    => 'required|numeric',
            'due_date'           => 'required|date',
            'sub_total'         => 'required|numeric',
            'discount_type'      => 'required|max:50',
            'discount'           => 'required|numeric|min:0',
            'discount_amount'    => 'required|numeric|min:0',
            'shipping_charges'   => 'required|numeric|min:0',
            'hallmark_charges'   => 'required|numeric|min:0',
            'packing_charges'    => 'required|numeric|min:0',
            'tax_id'             => 'required|numeric',
            'tax_amount'         => 'required|numeric',
            'total_amount'       => 'required|numeric',
            'round_off'          => 'required|numeric',
            'grand_total'        => 'required|numeric',
            'term_id'            => 'sometimes|nullable|numeric',
            'terms'              => 'max:255',
            'note'               => 'max:255',
        ]);

        $preference        = Preference::first();
        $customer          = Customer::where('customer_id',$request->customer_id)->first();
        $invoice           = Invoice::where('invoice_id',$request->invoice_id)->first();
        $invoice_products  = Invoice::where('invoice_id',$request->invoice_id)->get();

        $data['reference_date'] = $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $invoice->reference_date;

        $collect = collect($request->invoice_products);

        
        $sub_total_check   = $collect->reduce(function ($carry, $item) {
                          return $carry + $item['sub_total'];
                      });

        if ($sub_total_check > 0) {
        
            Invoice::where('invoice_id',$invoice_id)->update($data);

            //Selected Invoice Products Delete
            foreach ($request->deleted_invoice_products as $invoice_product) 
            {
                InvoiceProduct::where('invoice_product_id',$invoice_product['invoice_product_id'])->delete();
            }

            foreach ($request->invoice_products as $invoice_product) 
            {
                $product = Product::where('product_id',$invoice_product['product_id'])->first();

                $data = [
                    'invoice_id'         => $invoice['invoice_id'],
                    'classification_id'  => $invoice_product['classification_id'],
                    'category_id'        => $invoice_product['category_id'],
                    'subcategory_id'     => $invoice_product['subcategory_id'],
                    'product_id'         => $invoice_product['product_id'],
                    'product_type'       => $invoice_product['product_type'],
                    'product_code'       => $invoice_product['product_code'],
                    'hsn_code'           => $product->hsn_code,
                    'product_name'       => $invoice_product['product_name'],
                    'description'        => $product->description,
                    'size'               => $product->size,
                    'stone_color'        => $product->stone_color,
                    'purity'             => $product->purity,
                    'special_product'    => $product->special_product,
                    'unit'               => $invoice_product['unit'],
                    'gross_weight'       => $invoice_product['gross_weight'],
                    'cover_weight'       => $product['cover_weight'],
                    'net_weight'         => $invoice_product['net_weight'],
                    'pieces'             => $invoice_product['pieces'],
                    'silver_rate'        => $invoice_product['silver_rate'],
                    'product_price_id'   => $invoice_product['product_price_id'], 
                    'customer_type'      => $invoice_product['customer_type'], 
                    'making_charges'     => $invoice_product['making_charges'],
                    'net_rate'           => $invoice_product['net_rate'],
                    'piece_rate'         => $invoice_product['piece_rate'],
                    'amount'             => $invoice_product['amount'],
                    'other_charges'      => $invoice_product['other_charges'],
                    'box_code'           => $invoice_product['box_code'],
                    'box_rate'           => $invoice_product['box_rate'],
                    'box_amount'         => $invoice_product['box_amount'],
                    'sub_total'          => $invoice_product['sub_total'],
                ];

                if(empty($invoice_product['invoice_product_id'])) {
                    $invoice_prod_up = InvoiceProduct::create($data);
                }else{
                    $invoice_prod_up = InvoiceProduct::where('invoice_product_id',$invoice_product['invoice_product_id'])->update($data);
                }
            }
            
        }else{
            $data = $this->validate($request, [
                'invoice_products'  => 'required',
                'classification_id' => 'required',
                'category_id'       => 'required',
                'subcategory_id'    => 'required',
                'product_id'        => 'required',
            ],[
                'classification_id.required' => 'Add Invoice Products Or delete Invoice',
                'category_id.required' => 'Add Invoice Products Or delete Invoice',
                'subcategory_id.required' => 'Add Invoice Products Or delete Invoice',
                'product_id.required' => 'Add Invoice Products Or delete Invoice',
            ]);
        }

    }
   
    public function destroy($invoice_id)
    {
        InvoiceProduct::where('invoice_id',$invoice_id)->delete();
        Invoice::where('invoice_id',$invoice_id)->delete();
    }

    public function preview($invoice_id,$display)
    {

        $org = Organization::first();
        $invoice = Invoice::where('invoice_id',$invoice_id)->with('Customer','InvoiceProducts')->first();

        if ($display == 'Work Sheet') {
            PDF::loadView('sales.invoice.pdf1', compact('invoice','org'), [], [
                'margin_top' => 46.8,
                'format'    => 'A4-L',
            ])->stream($invoice->invoice_no.'.pdf');
        }else{
            PDF::loadView('sales.invoice.pdf2', compact('invoice','org'), [], [
                'margin_top' => 15.5,
            ])->stream($invoice->invoice_no.'.pdf');
        }
        
    }

    public function invoice_mail(Request $request)
    {
        $data = $this->validate($request, [
            'invoice_id'    => 'required|numeric',
            'to'            => 'required|email',
            'subject'       => 'required|max:50',
            'body'          => 'required|max:2550',
        ]);

        $org = Organization::first();
        $invoice = Invoice::where('invoice_id',$request->invoice_id)->first();

        // $pdf = PDF::loadView('sales.invoice.pdf1', compact('org','invoice'), [], [
        //     'isRemoteEnabled' => true,
        //     'margin_top' => 41.8,
        //     'format' => 'A4-L'
        // ]);

        // Mail::send('mail.invoice_mail', $data, function($message) use ($request,$pdf,$org,$invoice) {
        //     $message->to($request->to, $invoice->Customer->customer_name);
        //     $message->subject($request->subject);
        //     $message->attachData($pdf->output(), $invoice->invoice_no.'.pdf');
        //     $message->from($org->email,$org->org_name);
        // });

        Mail::send(new NewInvoice($invoice,$request,$org));
    }

    public function get_unpaid_invoices(Request $request)
    {
        $invoices =  Invoice::where('customer_id',$request->customer_id)->get();
        $result = [];
        foreach ($invoices as $invoice) 
        {
            $paid_amount = ReceiptParticular::where('reference_id',$invoice->invoice_id)->sum('paid_amount');
            $due_amount = $invoice->grand_total - $paid_amount;
            if($due_amount!=0)
            {
                $data = [
                    'check'          => false,
                    'invoice_id'     => $invoice->invoice_id,
                    'invoice_no'     => $invoice->invoice_no,
                    'invoice_date'   => $invoice->invoice_date,
                    'grand_total'    => $invoice->grand_total,
                    'paid_amount'    => $paid_amount,
                    'due_amount'     => $due_amount,
                    'payable_amount' => 0.00,
                    //'payable_amount' => $due_amount,
                ];
                array_push($result, $data);
            }
        }
        return $result;
    }

    public function convert_indent(Request $request)
    {
        $indent_products = IndentProduct::whereIn('indent_product_id',$request->input())->with('Indent','Tax')->get();
        $indent          = Indent::where('indent_id',$indent_products[0]->indent_id)->first();
        $employee        = Employee::where('employee_id',$indent->employee_id)->first();
        $result          = [];
        $final_result    = [];
        foreach ($indent_products as $key => $indent_product) {

            $product = Product::where('product_id',$indent_product->product_id)
                                ->where('category_id',$indent_product->category_id)
                                ->where('subcategory_id',$indent_product->subcategory_id)
                                ->with('ProductPrice','Category','Subcategory')
                                ->first();
            if($product->unit=='Weight')
            {
                $amount     = ($indent_product->making_charges*$product->net_weight);
                $net_rate   = $product->net_weight;
                $gross_weight = $product->gross_weight;
                $net_weight   = $product->net_weight;
            }else{
                $amount     = 0;
                $net_rate   = 0;
                if($product->gross_weight!=''){
                    $gross_weight = $product->gross_weight;
                }else{
                    $gross_weight = 0;
                }
                if($product->net_weight!=''){
                    $net_weight = $product->net_weight;
                }else{
                    $net_weight = 0;
                }
            }
                $data       = [
                    'indent_product_id'         => $indent_product->indent_product_id,
                    'classification'            => $product->Classification,
                    'classification_id'         => $product->classification_id,
                    'classification_name'       => $product->Classification->classification_name,
                    'category_id'               => $product->category_id,
                    'category_name'             => $product->Category->category_name,
                    'subcategory_id'            => $product->subcategory_id,
                    'subcategory_name'          => $product->Subcategory->subcategory_name,
                    'product_id'                => $product->product_id,
                    'product_type'              => $product->product_type,
                    'product_code'              => $product->product_code,
                    'product_name'              => $product->product_name,
                    'unit'                      => $product->unit,
                    'gross_weight'              => $gross_weight,
                    'cover_weight'              => $product->cover_weight,
                    'piece_rate'                => $product->piece_rate,
                    'net_weight'                => $net_weight,
                    'pieces'                    => $indent_product->quantity,
                    'silver_rate'               => 0,
                    'product_price_id'          => $product->ProductPrice->product_price_id,
                    'customer_type'             => $product->ProductPrice->customer_type,
                    'making_charges'            => $indent_product->making_charges,
                    'net_rate'                  => $net_rate,
                    'amount'                    => $amount,
                    'other_charges'             => 0,
                    'box_code'                  => $product->ProductPrice->box_code,
                    'box_rate'                  => $product->ProductPrice->box_rate,
                    'box_amount'                => 0,
                    'sub_total'                 => $amount,
                    'key'                       => '',
                    'status'                    => 0,
                    'type'                      => 'Sales',
                ];
            array_push($result, $data);
        }
        $final_result = [
            'indent'  => $indent_products,
            'data'    => $result,
            'employee'=> $employee,
        ];
        return $final_result;
    }

    public function convert_proforma(Request $request)
    {
        return ProformaProduct::whereIn('proforma_product_id',$request->input())->with('Proforma','Category','Subcategory','Tax')->get();
    }
}

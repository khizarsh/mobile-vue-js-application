<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Organization;
use App\User;

use App\Customer;
use App\Supplier;
use App\Invoice;
use App\Bill;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $org = Organization::first();

        return view('home',compact('org'));
    }

    public function get_dashboard_data(Request $request)
    {
        $data = [];
        $data['users']      = User::count();
        $data['customers']  = Customer::count();
        $data['suppliers']  = Supplier::count();
        $data['bills']      = Bill::count();
        $data['invoices']   = Invoice::count();

        return $data;
    }

    public function payment($month,$year)
    {
        return Payment::whereYear('voucher_date',$year)
                        ->whereMonth('voucher_date',$month)
                        ->sum('total_amount');
    }

    public function receipt($month,$year)
    {
       return Receipt::whereYear('receipt_date',$year)
                    ->whereMonth('receipt_date',$month)
                    ->sum('total_amount');
    }

    public function copy()
    {
        $this->copy_primary_images();
        $this->copy_secondary_images();
        $this->copy_product_prices();
    }

    public function copy_primary_images()
    {
        $products = \App\Product::get();
        foreach ($products as $product) 
        {
            $a=array("1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg");
            $random_keys=array_rand($a);

            \App\ProductImage::create([
                'product_id' => $product->product_id,
                'image_type' => 'Primary Image',
                'image' => $a[$random_keys],
            ]);
        }
        return redirect('/');
    }
    public function copy_secondary_images()
    {
        $products = \App\Product::get();
        foreach ($products as $product) 
        {
            for ($i=0; $i<5; $i++) 
            { 
                $a=array("1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg");
                $random_keys=array_rand($a);

                \App\ProductImage::create([
                    'product_id' => $product->product_id,
                    'image_type' => 'Secondary Image',
                    'image' => $a[$random_keys],
                ]);
            }
        }
        return redirect('/');
    }

    public function copy_product_prices()
    {
        $products = \App\Product::get();
        foreach ($products as $product) 
        {
            $a=array("Retailer","Corporate","Wholesales");
            for ($i=0; $i<=2; $i++) 
            {
                \App\ProductPrice::create([
                    'product_id' => $product->product_id,
                    'customer_type' => $a[$i],
                    'making_charges' => rand(1000,10000),
                    'box_code' => 'BOX-'.rand(),
                    'box_rate' => rand(100,1000),
                ]);
            }
        }
        return redirect('/');
    }
}

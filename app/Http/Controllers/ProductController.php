<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Category;
use App\Subcategory;
use App\Supplier;
use App\ProductImage;
use App\ProductPrice;

use App\Module\Module;
use PDF;
use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;

use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return Product::where('product_code', 'like', '%'.$request->search.'%')
            ->orWhere('product_name', 'like', '%'.$request->search.'%')
            ->orWhere('factory_code', 'like', '%'.$request->search.'%')
            ->orWhere('gross_weight', 'like', '%'.$request->search.'%')
            ->orWhere('cover_weight', 'like', '%'.$request->search.'%')
            ->orWhere('net_weight', 'like', '%'.$request->search.'%')
            ->orWhere('unit', 'like', '%'.$request->search.'%')
            ->orWhere('opening_stock', 'like', '%'.$request->search.'%')
            ->orWhereHas('Classification',function($value) use($request){
                $value->where('classification_name', 'like', '%'.$request->search.'%');
            })
            ->orWhereHas('Category',function($value) use($request){
                $value->where('category_name', 'like', '%'.$request->search.'%');
            })
            ->orWhereHas('Subcategory',function($value) use($request){
                $value->where('subcategory_name', 'like', '%'.$request->search.'%');
            })
            ->orWhereHas('Supplier',function($value) use($request){
                $value->where('supplier_name', 'like', '%'.$request->search.'%');
            })->with('Classification','Category','Subcategory','Supplier')
            ->paginate(10); 
    }

    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'product_type'       => 'required|max:50',
            'product_code'       => 'required|max:50|unique:products',
            'factory_code'       => 'nullable|max:50',
            'hsn_code'           => 'sometimes|nullable|max:50',
            'product_name'       => 'required',
            'classification_id'  => 'required',
            'category_id'        => 'required',
            'subcategory_id'     => 'required',
            'description'        => 'sometimes|nullable',
            'unit'               => 'required|max:50',
            'gross_weight'       => 'sometimes|nullable|max:50',
            'piece_rate'         => 'sometimes|nullable|max:50',
            'cover_weight'       => 'required|max:50',
            'net_weight'         => 'sometimes|nullable|max:50',
            'size'               => 'sometimes|nullable|max:50',
            'stone_color'        => 'sometimes|nullable|max:50',
            'purity'             => 'sometimes',
            'special_product'    => 'required',
            'reorder_time'       => 'nullable|sometimes',
            'reorder_level'      => 'required',
            'opening_stock'      => 'required|numeric',
            'opening_stock_as_on'=> 'required|date|after_or_equal:'.date('Y-m-d'),
            'supplier_id'        => 'sometimes|nullable',
        ]);

        $this->validate($request,['primary_image' => 'required']);

        if($request->unit=="Weight")
        {
            $this->validate($request,[
                'gross_weight'  => 'required',
                'net_weight'    => 'required',
            ]);
        }


        $product_prices_count = count($request->product_prices);
        if($product_prices_count>0)
        {
            $product = Product::create($data);

            $data = $request->primary_image;
        
            if (strpos($data, 'base64') !== false) 
            {

                $image_parts = explode(";base64,", $request->primary_image);
                $image_base64 = base64_decode($image_parts[1]);
                $image_type_aux = explode("image/", $image_parts[0]);
                $png_url = date('Ymdhis').".".$image_type_aux[1];
                $path = public_path().'/storage/products/' . $png_url;
                file_put_contents($path, $image_base64);

                $product_image   =  ProductImage::create([
                    'product_id' => $product->product_id,
                    'image'      => $png_url,
                    'image_type' => 'Primary Image',
                ]);
            }
            foreach ($request->product_prices as $key => $product_price) {
                $product_price_up    =  ProductPrice::create([
                'product_id'        => $product->product_id,
                'customer_type'     => $product_price['customer_type'],
                'making_charges'    => $product_price['making_charges'],
                'box_code'          => $product_price['box_code'],
                'box_rate'          => $product_price['box_rate'],
            ]);
            }
        }else{
            $data = $this->validate($request, [
                'customer_type'     => 'required',
                'making_charges'    => 'required',
                'box_code'          => 'nullable',
                'box_rate'          => 'required',
            ],[
                'product_id.required' => 'Add Products For Customer Type'
            ]);
        }
        
        return $product;
    }

    public function show($product_id)
    {
        return Product::where('product_id',$product_id)->with('Classification','Category','Subcategory','ProductPrices','Supplier','ProductImages','latestPrimaryImage')->first();
    }

    public function update(Request $request,Product $product)
    {
        $data = $this->validate($request,[
            'product_type'       => 'required|max:50',
            'product_code'       => 'required|max:50|unique:products,product_code,'.$request->product_id.',product_id',
            'factory_code'       => 'nullable|max:50',
            'hsn_code'           => 'sometimes|nullable|max:50',
            'product_name'       => 'required',
            'classification_id'  => 'required',
            'category_id'        => 'required',
            'subcategory_id'     => 'required',
            'description'        => 'sometimes|nullable',
            'unit'               => 'required|max:50',
            'gross_weight'       => 'sometimes|nullable|max:50',
            'piece_rate'         => 'sometimes|nullable|max:50',
            'cover_weight'       => 'required|max:50',
            'net_weight'         => 'sometimes|nullable|max:50',
            'size'               => 'sometimes|nullable|max:50',
            'stone_color'        => 'sometimes|nullable|max:50',
            'purity'             => 'sometimes',
            'special_product'    => 'required',
            'reorder_time'       => 'nullable|sometimes',
            'reorder_level'      => 'required',
            'opening_stock'      => 'required|numeric',
            'opening_stock_as_on'=> 'required|date|after_or_equal:'.$product->opening_stock_as_on,
            'supplier_id'        => 'sometimes|nullable',
        ]);

        $this->validate($request,['primary_image' => 'required']);

        Product::where('product_id',$product->product_id)->update($data);

         $data = $request->primary_image;
        
        if (strpos($data, 'base64') !== false) 
        {

            $image_parts = explode(";base64,", $request->primary_image);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/products/' . $png_url;
            file_put_contents($path, $image_base64);

            $product_image   =  ProductImage::where('product_id',$product->product_id)
                                        ->where('image_type','Primary Image')
                                        ->update(['image' => $png_url ]);
        }

        //Selected Product Price
        $product_prices_count = count($request->deleted_product_prices);
        if($product_prices_count>0)
        {
            foreach ($request->deleted_product_prices as $product_price) 
            {
                ProductPrice::where('product_price_id',$product_price['product_price_id'])->delete();
            }
        }
       
        foreach ($request->product_prices as $product_price) 
        {
            $data = [
                'product_id'        => $product->product_id,
                'customer_type'     => $product_price['customer_type'],
                'making_charges'    => $product_price['making_charges'],
                'box_code'          => $product_price['box_code'],
                'box_rate'          => $product_price['box_rate'],
            ];

            if(empty($product_price['product_price_id'])) {
                $product_price_up = ProductPrice::create($data);
            }else{
                $product_price_up = ProductPrice::where('product_price_id',$product_price['product_price_id'])->update($data);
            }
        }
    }

    public function destroy($product_id)
    {
        ProductPrice::where('product_id',$product_id)->delete();
        Product::where('product_id',$product_id)->delete();
    }

    public function get_product_code()
    {
        $id = Product::withTrashed()->max('product_id');
        $product_code = $id + 1;
        $length = strlen($product_code);
        $n = 4 - $length;
        for ($i=0; $i < $n; $i++) 
        { 
            $product_code = '0'.$product_code;
        }
        return $product_code;
    }

    public function product_image(Request $request)
    {
        $this->validate($request,[
            'product_id' => 'required',
            'image'      => 'required',
            'image_type' => 'required',
        ]);

        if ($request->image_type == 'Primary Image') {
            $this->validate($request,[
                'image_type'  => 'required|unique:product_images,image_type,NULL,product_image_id,deleted_at,NULL,product_id,'.$request->product_id,
            ],[
                'image_type.unique' => 'The primary image has already been taken.'
            ]);
        }
        

        $data = $request->image;
        
        if (strpos($data, 'base64') !== false) {

            $image_parts = explode(";base64,", $request->image);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/products/' . $png_url;
            file_put_contents($path, $image_base64);

            $product_image   =  ProductImage::create([
                'product_id' => $request->product_id,
                'image'      => $png_url,
                'image_type' => $request->image_type,
            ]);
        }

        return $product_image;
    }

    public function product_image_delete(Request $request)
    {
        $prod_img = ProductImage::where('product_image_id',$request->product_image_id)->delete();

        if ($prod_img) {
            return $request;
        }

    }

    public function get_limit_products(Request $request)
    {
        $products = Product::where('subcategory_id',$request->subcategory_id)
                        ->where(function($query) use($request){
                            $query->where('product_code', 'like', '%'.$request->search.'%')
                            ->orWhere('product_name', 'like', '%'.$request->search.'%')
                            ->orWhere('hsn_code', 'like', '%'.$request->search.'%');
                        })->limit(5)
                        ->orderBy('product_name')
                        ->orderBy('product_code')
                        ->with('ProductPrices','ProductImage')
                        ->get();

        $products = $products->each(function($product) use($request){
            return $product['customer_product_price'] = $product->CustomerProductPrice($product->product_id,$request->customer_type);
        });

        return $products;
    }

    public function get_latest_product_price(Request $request)
    {
         return Product::where('subcategory_id',$request->subcategory_id)
                        ->where(function($query) use($request){
                            $query->where('product_code', 'like', '%'.$request->search.'%')
                            ->orWhere('product_name', 'like', '%'.$request->search.'%')
                            ->orWhere('hsn_code', 'like', '%'.$request->search.'%');
                        })->limit(5)
                        ->orderBy('product_name')
                        ->orderBy('product_code')
                        ->with('ProductPrice','ProductImage')
                        ->get();
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'category_id'        => 'required|numeric',
            'category_name'      => 'required',
            'subcategory_id'     => 'required|numeric',
            'subcategory_name'   => 'required',
            'product_id'         => 'required|numeric',
            'product_type'       => 'required',
            'product_code'       => 'required',
            'product_name'       => 'required',
            'gross_weight'       => 'required|numeric|min:0',
            'piece_rate'         => 'sometimes|nullable|max:50',
            'net_weight'         => 'required|numeric|min:0',
            'pieces'             => 'required|numeric|min:0',
            'silver_rate'        => 'required|numeric|min:0',
            'making_charges'     => 'required|numeric|min:0',
            'net_rate'           => 'required|numeric|min:0',
            'piece_rate'         => 'required|numeric|min:0',
            'amount'             => 'required|numeric|not_in:0',
            'other_charges'      => 'required|numeric|min:0',
            'box_code'           => 'nullable',
            'box_rate'           => 'required|numeric|min:0',
            'box_amount'         => 'required|numeric|min:0',
            'sub_total'          => 'required|numeric|not_in:0',
        ]);
        
        return $request;
    }

    public function auto_generate_no(Request $request)
    {
        $module = new Module();
        $auto_generate_no = $module->auto_generate_no($request->module,$request->module_id);

        return $auto_generate_no;

    }

    public function product_price_validation(Request $request)
    {
        $data = $this->validate($request,[
            'customer_type' => 'required',
            'making_charges'=> 'required|numeric',
            'box_code'      => 'nullable',
            'box_rate'      => 'required',
        ]);
        return $request;
    }

    public function product_catalog(Request $request)
    {
        // $this->validate($request, [
        //     'from_date' => 'required|date',
        //     'to_date'   => 'required|date',
        // ]);

        $from_date = Date('Y-m-d',strtotime($request->from_date));
        $to_date = Date('Y-m-d',strtotime($request->to_date));

        $products = Product::with('Category')->get();
        
        if ($request->category_id != '') {
            $products = $products->filter(function($query) use($request){
               return ($query->category_id == $request->category_id ); 
            })->values();   
        }

        if ($request->subcategory_id != '') {
            $products = $products->filter(function($query) use($request){
               return ($query->subcategory_id == $request->subcategory_id ); 
            })->values();   
        }

        // dd($request['unit']);

        if ($request->unit != '') {
            $products = $products->filter(function($query) use($request){
               return ($query->unit == $request->unit ); 
            })->values();   
        }

        if ($request->special_product != '') {
            $products = $products->filter(function($query) use($request){
               return ($query->special_product == $request->special_product ); 
            })->values();   
        }

        if ($request->supplier_id != '') {
            $products = $products->filter(function($query) use($request){
               return ($query->supplier_id == $request->supplier_id ); 
            })->values();   
        }

        switch ($request->display_type) {
            case 'pdf':
                PDF::loadView('catalogs.product', compact('request','products'), [], [
                'margin_top' => 10
                ])->stream('product-catalog.pdf');
                break;
            case 'excel':
                return view('catalogs.product',compact('request','products'));
                break;
            default:
                return view('catalogs.product',compact('request','products'));
                break;
        }
    }

    public function get_barcode_qrcode(Request $request)
    {
        $data = [];
        $data['barcode'] = DNS1D::getBarcodePNG($request->product_code, 'C39E');
        $data['qrcode']  = DNS2D::getBarcodePNG($request->product_code, 'QRCODE');
        return $data;
    }

    public function get_products(Request $request)
    {
        return Product::get();
    }

    public function import(Request $request) {
        $data = $this->validate($request, [
            'file' => 'required|file',
        ]);

        Excel::import(new ProductImport, $request->file('file'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Reference;
use App\Manage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        
        $user = User::where('user_role','<>','Super Admin')
                  ->where(function($value) use($request){
                    $value->where('username', 'like', '%'.$request->search.'%')
                  ->orWhere('email', 'like', '%'.$request->search.'%')
                  ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
                  ->orWhere('user_role', 'like', '%'.$request->search.'%');
            })->paginate(10);
        
        return $user;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username'      => 'required|max:50|unique:users',
            'email'     => 'required|email|max:50|unique:users',
            'password'  => 'required|min:6',
            'user_role' => 'required|max:50',
            'mobile_no' => 'required|digits:10|regex:/[0-9]{10}/|numeric',
            'address'   => 'required|max:255',
        ]);

        $user = User::create([
            'username'      => $request->username,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'user_role' => $request->user_role,
            'mobile_no' => $request->mobile_no,
            'address'   => $request->address,
        ]);

        $data = $request->avatar;
        
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->avatar);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/users/' . $png_url;
            file_put_contents($path, $image_base64);
            User::where('user_id',$user->user_id)->update([
                'avatar'    => $png_url,
            ]);
        }
    }

    public function edit(User $user)
    {
        return $user;
    }
    
    public function update(Request $request)
    {
        $this->validate($request, [
            'username'  => 'required|string|max:50|unique:users,username,'.$request->user_id.',user_id',
            'email'     => 'required|string|email|max:50|unique:users,email,'.$request->user_id.',user_id',
            'user_role' => 'required|max:50',
            'mobile_no' => 'required|max:13|regex:/[0-9]{10}/',
            'address'   => 'required|max:2550',
        ]);

        User::where('user_id',$request->user_id)->update([
            'username'      => $request->username,
            'email'     => $request->email,
            'user_role' => $request->user_role,
            'mobile_no' => $request->mobile_no,
            'address'   => $request->address,
        ]);

        // if (!empty($request->password)) {
        //     User::where('user_id',$request->user_id)->update([
        //         'password'      => Hash::make($request->password),
        //         'updated_by'    => Auth::User()->userusername,         
        //     ]);
        // }

        $data = $request->avatar;
        
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->avatar);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/users/' . $png_url;
            file_put_contents($path, $image_base64);
            User::where('user_id',$request->user_id)->update([
                'avatar'    => $png_url,
            ]);
        }
    }

    public function destroy(User $user)
    {
        User::where('user_id',$user->user_id)->delete();
    }

    public function get_users(Request $request)
    {
        switch ($request->type) {
            case 'employees':
                return User::where('user_role','Employee')->get();
                break;
            default:
                return User::where('user_id',Auth::user()->user_id)->first();
                break;
        }
    }

    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'username'  => 'required|string|max:50|unique:users,username,'.$request->user_id.',user_id',
            'email'     => 'required|string|email|max:50|unique:users,email,'.$request->user_id.',user_id',
            'mobile_no' => 'required|digits:10|regex:/[0-9]{10}/|numeric',
            'address'   => 'required|max:255',
        ]);

        User::where('user_id',$request->user_id)->update([
            'username'      => $request['username'],
            'email'     => $request['email'],
            'user_role' => $request['user_role'],
            'mobile_no' => $request['mobile_no'],
            'address'   => $request['address'],
        ]);

        if ($request->password !='' && $request->password_confirmation !='') {
           User::where('user_id',$request->user_id)->update([
                'password'      => Hash::make($request->password),
            ]);
        }

        $data = $request->avatar;
        
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->avatar);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/users/' . $png_url;
            file_put_contents($path, $image_base64);
            User::where('user_id',$request->user_id)->update([
                'avatar'    => $png_url,
            ]);
        }
        return User::where('user_id',$request->user_id)->first();
    }

    public function get_limit_users(Request $request)
    {
        $user_id = Auth::User()->user_id;

        if ($request->user_role == 'all') {
            return User::where([['user_id','<>',$user_id],['user_role','<>','Super Admin']])
                    ->where(function($query) use($request){
                        $query->where('username','like',"%$request->search%")
                            ->orWhere('email', 'like', "%$request->search%")
                            ->orWhere('mobile_no', 'like', "%$request->search%");
                    })->limit(10)
                    ->orderBy('username')
                    ->get();
        }else{
            return User::where([['user_id','<>',$user_id],['user_role','<>','Super Admin'],['user_role',$request->user_role]])
                    ->where(function($query) use($request){
                        $query->where('username','like',"%$request->search%")
                            ->orWhere('email', 'like', "%$request->search%")
                            ->orWhere('mobile_no', 'like', "%$request->search%");
                    })->limit(10)
                    ->orderBy('username')
                    ->get();
        }
    }

}

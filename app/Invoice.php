<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    protected $guarded = [];

    protected $primaryKey = 'invoice_id';

    public function Customer()
    {
    	return $this->hasOne('App\Customer','customer_id','customer_id')->withTrashed();
    }

    public function User()
    {
        return $this->hasOne('App\User','user_id','user_id');
    }


    public function Term()
    {
    	return $this->hasOne('App\Term','term_id','term_id')->withTrashed();
    }

    public function InvoiceProducts()
    {
        return $this->hasMany('App\InvoiceProduct','invoice_id','invoice_id')->with('Category');
    }

    public function Tax()
    {
        return $this->hasOne('App\Tax','tax_id','tax_id');
    }

    public function SubTotal($invoice_id)
    {
        $sub_total = round(InvoiceProduct::where('invoice_id',$invoice_id)->sum('amount'),2);
        return $sub_total;
    }

    public function Discount($invoice_id)
    {
        $discount = round(InvoiceProduct::where('invoice_id',$invoice_id)->sum('discount_amount'),2);
        return $discount;
    }

    public function TotalTax($invoice_id)
    {
        $total_tax = round(InvoiceProduct::where('invoice_id',$invoice_id)->sum('tax_amount'),2);
        return $total_tax;
    }

    public function Taxes($invoice_id)
    {
        return InvoiceProduct::where('invoice_id',$invoice_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }

    public function TaxableValue($invoice_id,$tax_id)
    {
        $amount =  round(InvoiceProduct::where([['invoice_id',$invoice_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(InvoiceProduct::where([['invoice_id',$invoice_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

        //return round($amount-$discount);
        return round($amount);
    }

}

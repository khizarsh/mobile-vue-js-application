<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->string('username',50)->unique();
            $table->string('email',50)->unique();
            $table->string('password',255);
            $table->string('user_role',50);
            $table->string('mobile_no', 20);
            $table->text('address');
            $table->string('avatar',50)->default('avatar.png');
            $table->timestamps();
        });

         DB::table('users')->insert(
            [
                [                    
                    'username' => 'shareef',
                    'email' => 'shareef@gmail.com',
                    'password' => Hash::make('qwerty'),
                    'user_role' => 'Super Admin',
                    'mobile_no' => '7406262370',
                    'address'   => '#1920, Halle Wood Yard Road, Mysore - 570020',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

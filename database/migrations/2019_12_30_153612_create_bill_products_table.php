<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_products', function (Blueprint $table) {
            $table->bigIncrements('bill_product_id');
            $table->bigInteger('bill_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->longtext('product_name');
            $table->longtext('emi_number',50);
            $table->longtext('description')->nullable();
            $table->string('quantity',50);
            $table->double('amount',15,2)->nullable();
            $table->double('sub_total',15,2)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('bill_id')->references('bill_id')->on('bills');
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('category_id')->references('category_id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_products');
    }
}

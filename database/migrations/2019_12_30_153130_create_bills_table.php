<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('bill_id');
            $table->string('bill_no',50);
            $table->date('bill_date');
            $table->double('sub_total',15,2);
            $table->string('discount_type',50);
            $table->double('discount',15,2);
            $table->double('discount_amount',15,2);
            $table->bigInteger('tax_id')->unsigned();
            $table->double('tax_amount',15,2);
            $table->double('total_amount',15,2);
            $table->double('round_off',15,2);
            $table->double('grand_total',15,2);
            $table->bigInteger('term_id')->unsigned()->nullable();
            $table->longtext('terms')->nullable();
            $table->longtext('note')->nullable();
            $table->timestamps();
            $table->foreign('term_id')->references('term_id')->on('terms');
            $table->foreign('tax_id')->references('tax_id')->on('taxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}

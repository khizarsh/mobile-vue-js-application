<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Customer::class,1000)->create();
        factory(App\Lead::class,1000)->create();
        factory(App\Supplier::class,1000)->create();
        factory(App\Product::class,1000)->create();
    }
}

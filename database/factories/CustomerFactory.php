<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'customer_type' => $faker->randomElement($array = array ('Retailer','Corporate','Wholesales')),
        'customer_code' => $faker->unique()->numberBetween(1000,100000),
        'customer_name' => $faker->company,
        'contact_person'	=> $faker->name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->email,
        'password' => $faker->password,
        'mobile_no' => $faker->phoneNumber,
        'phone_no' => $faker->phoneNumber,
        'billing_address' => $faker->address,
        'shipping_address' => $faker->address,
        'city' => $faker->city,
        'pincode' => '570006',
        'payment_term_id' => App\PaymentTerm::all()->random()->payment_term_id,
        'place_id' => App\Place::all()->random()->place_id,
    ];
});

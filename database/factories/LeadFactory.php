<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lead;
use Faker\Generator as Faker;

$factory->define(Lead::class, function (Faker $faker) {
    return [
        'lead_type' => $faker->randomElement($array = array ('New Lead','Old Lead','Potential Lead')),
        'name'	=> $faker->name,
        'mobile_no' => $faker->phoneNumber,
        'phone_no' => $faker->phoneNumber,
        'email' => $faker->unique()->email,
        'address' => $faker->address,
        'note' => $faker->address,
        'land_mark' => $faker->city,
        'pincode' => '570006',
    ];
});

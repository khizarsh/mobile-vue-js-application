<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
	$classification_id = App\Classification::get()->random()->classification_id;
	$category_id = App\Category::where('classification_id',$classification_id)->get()->random()->category_id;
	$subcategory_id = App\Subcategory::where('category_id',$category_id)->get()->random()->subcategory_id;
	$gross_weight = $faker->numberBetween(100,300);
	$cover_weight = $faker->numberBetween(10,30);
	$net_weight = $gross_weight + $cover_weight;
    return [
        'product_type' => 'Goods',
        'product_code'	=> $faker->unique()->numberBetween(1000,100000),
        'hsn_code'	=> $faker->randomElement($array = array ('54677','132232','76098','6743564','348765')),
        'product_name' => $faker->unique()->name,
        'factory_code' => $faker->numberBetween(1000,2000),
        'classification_id' => $classification_id,
        'category_id' => $category_id,
        'subcategory_id' => $subcategory_id,
        'description' => $faker->text,
        'unit'	=> $faker->randomElement($array = array ('Weight','Pieces')),
        'gross_weight' => $gross_weight,
        'cover_weight' => $cover_weight,
        'net_weight' => $net_weight,
        'piece_rate' => $faker->numberBetween(1,10000),
        'size' => $faker->numberBetween(10,100),
        'stone_color'	=> $faker->randomElement($array = array ('Red','Green','Yellow','Gold','Silver','Blue','Purple','Black')),
        'purity' => $faker->numberBetween(10,100),
        'special_product' => $faker->randomElement($array = array ('Yes','No')),
        'reorder_level' => $faker->numberBetween(10,100),
        'reorder_time' => $faker->numberBetween(1,30),
        'opening_stock' => $faker->numberBetween(100,1000),
        'opening_stock_as_on' => '2020-06-01',
        'supplier_id' => App\Supplier::all()->random()->supplier_id,
    ];
});

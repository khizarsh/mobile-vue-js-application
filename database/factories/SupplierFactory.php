<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Supplier;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'supplier_code' => $faker->unique()->numberBetween(1000,100000),
        'supplier_name' => $faker->company,
        'email' => $faker->unique()->email,
        'mobile_no' => $faker->phoneNumber,
        'phone_no' => $faker->phoneNumber,
        'billing_address' => $faker->address,
        'shipping_address' => $faker->address,
        'payment_term_id' => App\PaymentTerm::all()->random()->payment_term_id,
        'place_id' => App\Place::all()->random()->place_id,
    ];
});

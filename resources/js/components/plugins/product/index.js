import Product from './Product.vue'

export {
    Product
}



// <!-- <b-modal id="product" title="Product" @ok="addProduct">
//             <div class="row">
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="product_type">Product Type *</label>
//                         <select class="form-control" :class="{'is-invalid': errors.product_type}" v-model="product.product_type" ref="product_type">
//                             <option value="">Select Product Type</option>
//                             <option value="Goods">Goods</option>
//                             <option value="Service">Service</option>
//                         </select>
//                         <div v-if="errors.product_type" class="invalid-feedback">{{ errors.product_type[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="product_code">Product code *</label>
//                         <input class="form-control" type="text"  :class="{'is-invalid': errors.product_code}" v-model="product.product_code"  placeholder="Product Code" >
//                         <div v-if="errors.product_code" class="invalid-feedback">{{ errors.product_code[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="hsn_code">HSN code</label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.hsn_code}" v-model="product.hsn_code" placeholder="HSN Code">
//                         <div v-if="errors.hsn_code" class="invalid-feedback">{{ errors.hsn_code[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="category_id">Category *</label>
//                         <select class="form-control" :class="{'is-invalid': errors.category_id}" v-model="product.category_id" @click="getCategories()">
//                             <option value="">Select Category</option>
//                             <option v-for="category in categories" :value="category.category_id">{{ category.category_name }}</option>
//                         </select>
//                         <div v-if="errors.category_id" class="invalid-feedback">{{ errors.category_id[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="product_name">Product Name *</label>
//                         <textarea class="form-control" :class="{'is-invalid': errors.product_name}" v-model="product.product_name" placeholder="Product Name" ref="product_name"></textarea>
//                         <div v-if="errors.product_name" class="invalid-feedback">{{ errors.product_name[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="description">Description</label>
//                         <textarea class="form-control" :class="{'is-invalid': errors.description}" v-model="product.description" placeholder="Description"></textarea>
//                         <div v-if="errors.description" class="invalid-feedback">{{ errors.description[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="product_unit">Product Unit </label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.product_unit}" v-model="product.product_unit" placeholder="Product Unit" >
//                         <div v-if="errors.product_unit" class="invalid-feedback">{{ errors.product_unit[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="purchase_rate_exc">Purchase Rate(Tax Exclusive) *</label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.purchase_rate_exc}" v-model="product.purchase_rate_exc" placeholder="Purchase Rate(Tax Exclusive)" @keyup="calPurcahseRateInc()">
//                         <div v-if="errors.purchase_rate_exc" class="invalid-feedback">{{ errors.purchase_rate_exc[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="sales_rate_exc">Sales Rate(Tax Exclusive)* </label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.sales_rate_exc}" v-model="product.sales_rate_exc" placeholder="Sales Rate(Tax Exclusive)" @keyup="calSalesRateInc()">
//                         <div v-if="errors.sales_rate_exc" class="invalid-feedback">{{ errors.sales_rate_exc[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="tax_id">Tax *</label>
//                         <select class="form-control" :class="{'is-invalid': errors.tax_id}" v-model="product.tax_id" @change="calPurcahseRateInc()">
//                             <option value="">Select Tax</option>
//                             <option v-for="tax in taxes" :value="tax.tax_id">{{ tax.tax_name }}</option>
//                         </select>
//                         <div v-if="errors.tax_id" class="invalid-feedback">{{ errors.tax_id[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="purchase_rate_inc">Purchase Rate(Tax Inclusive) *</label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.purchase_rate_inc}" v-model="product.purchase_rate_inc" placeholder="Purchase Rate(Tax Inclusive)" >
//                         <div v-if="errors.purchase_rate_inc" class="invalid-feedback">{{ errors.purchase_rate_inc[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="sales_rate_inc">Sales Rate(Tax Inclusive) *</label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.sales_rate_inc}" v-model="product.sales_rate_inc" placeholder="Sales Rate(Tax Inclusive)" >
//                         <div v-if="errors.sales_rate_inc" class="invalid-feedback">{{ errors.sales_rate_inc[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="min_stock">Min Stock </label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.min_stock}" v-model="product.min_stock" placeholder="Min Stock" >
//                         <div v-if="errors.min_stock" class="invalid-feedback">{{ errors.min_stock[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="vendor_id">Vendor </label>
//                         <select class="form-control" :class="{'is-invalid': errors.vendor_id}" v-model="product.vendor_id" @click='getVendors()'>
//                             <option value="">Select Supplier</option>
//                             <option v-for="contact in contacts" :value="contact.contact_id">{{ contact.contact_name }}</option>
//                         </select>
//                         <div v-if="errors.vendor_id" class="invalid-feedback">{{ errors.vendor_id[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="opening_stock">Opening Stock * </label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.opening_stock}" v-model="product.opening_stock" placeholder="Opening Stock" >
//                         <div v-if="errors.opening_stock" class="invalid-feedback">{{ errors.opening_stock[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="opening_stock_rate">Opening Stock Rate *</label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.opening_stock_rate}" v-model="product.opening_stock_rate" placeholder="Opening Stock Rate" >
//                         <div v-if="errors.opening_stock_rate" class="invalid-feedback">{{ errors.opening_stock_rate[0] }}</div>
//                     </div>
//                 </div>
//                 <div class="col-sm-6">
//                     <div class="form-group">
//                         <label for="size">Size</label>
//                         <input class="form-control" type="text" :class="{'is-invalid': errors.size}" v-model="product.size" placeholder="Opening Stock Rate" >
//                         <div v-if="errors.size" class="invalid-feedback">{{ errors.size[0] }}</div>
//                     </div>
//                 </div>
//             </div>
//         </b-modal> -->
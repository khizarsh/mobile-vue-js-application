require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

import moment from 'moment'
Vue.use(require('vue-moment'));
window.moment = require('moment');

// window.myModule = require('./modal');

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import vDialogs from 'v-dialogs';
Vue.use(vDialogs);

import { PaginationPlugin } from 'bootstrap-vue'
Vue.use(PaginationPlugin)

import { ModalPlugin } from 'bootstrap-vue';
Vue.use(ModalPlugin);

import { CollapsePlugin } from 'bootstrap-vue';
Vue.use(CollapsePlugin);

import { TooltipPlugin } from 'bootstrap-vue';
Vue.use(TooltipPlugin);

Vue.filter('redArr',function(value,field){
    let reduce_value = _.reduce(value,function(sum,val){
        return parseFloat(sum)+parseFloat(val[field]);
    },0);
    let reduced = parseFloat(reduce_value).toFixed(2);
    return reduced;
});

Vue.filter('tofix',function(value){
    let fixed = parseFloat((parseFloat(value)*1)).toFixed(2);
    return fixed;
});

let Myheader = require('./components/Header.vue').default;
let Mymenu = require('./components/Menu.vue').default;
let Myfooter = require('./components/Footer.vue').default;
let Report = require('./components/Report.vue').default;

let Home = require('./components/Home.vue').default;
let Configuration = require('./components/Configuration.vue').default;
let Organization = require('./components/configuration/Organization.vue').default;
let Tax = require('./components/configuration/Tax.vue').default;
let Term = require('./components/configuration/Term.vue').default;

//Users
let CreateUser = require('./components/people/user/Create.vue').default;
let DisplayUser = require('./components/people/user/Display.vue').default;
let ShowUser = require('./components/people/user/Show.vue').default;
let Profile = require('./components/people/user/Profile.vue').default;



//Suppliers
let suppliers = require('./components/people/supplier/index.vue').default;
let create_supplier = require('./components/people/supplier/create.vue').default;
let show_supplier = require('./components/people/supplier/show.vue').default;


//Customers
let customers = require('./components/people/customer/index.vue').default;
let create_customer = require('./components/people/customer/create.vue').default;
let show_customer = require('./components/people/customer/show.vue').default;
let preview_document = require('./components/people/customer/preview.vue').default;
let customers_report = require('./components/people/customer/report.vue').default;

//Products
let categories = require('./components/product/category.vue').default;
let products = require('./components/product/index.vue').default;
let create_product = require('./components/product/create.vue').default;
let show_product = require('./components/product/show.vue').default;

//Bills
let bills = require('./components/purchase/bill/index.vue').default;
let create_bill = require('./components/purchase/bill/create.vue').default;
let show_bill = require('./components/purchase/bill/show.vue').default;
let preview_bill = require('./components/purchase/bill/preview.vue').default;



//Invoices
let invoices = require('./components/sales/invoice/index.vue').default;
let create_invoice = require('./components/sales/invoice/create.vue').default;
let show_invoice = require('./components/sales/invoice/show.vue').default;
let preview_invoice = require('./components/sales/invoice/preview.vue').default;

//Reports
let invoice_report       = require('./components/report/sales/invoice.vue').default;
let invoice_tax          = require('./components/report/sales/invoice_gst.vue').default;
let bill_report          = require('./components/report/purchase/bill.vue').default;
let bill_tax             = require('./components/report/purchase/bill_gst.vue').default;
let stock_report         = require('./components/report/inventory/stock.vue').default;

const routes = [
	{ path: '/', component: Home, name: 'home'},
    { path: '/home', component: Home, name: 'home'},

    //Configuration
    { path: '/configuration', name:'configuration', component: Configuration },
    { path: '/organization', name:'organization', component: Organization },
    { path: '/tax', name:'tax', component: Tax },
    { path: '/term', name:'term', component: Term },
   

    //User
    { path: '/user/create', name:'user-create', component: CreateUser },
    { path: '/user/display', name:'user-display', component: DisplayUser },
    { path: '/user/edit/:user_id', name:'user-edit', component:CreateUser },
    { path: '/user/:user_id', name:'user.show', component:ShowUser },
    { path: '/profile',component:Profile },

   

    //Suppliers
    { path: '/suppliers', name:'suppliers.index', component: suppliers },
    { path: '/suppliers/create', name:'suppliers.create', component: create_supplier },
    { path: '/suppliers/:supplier_id/edit', name:'suppliers.edit', component:create_supplier },
    { path: '/suppliers/:supplier_id', name:'suppliers.show', component:show_supplier },

    //Products
    { path: '/categories', name:'categories', component: categories },
    { path: '/products', name:'products.index', component: products },
    { path: '/products/create', name:'products.create', component: create_product },
    { path: '/products/:product_id/edit', name:'products.edit', component:create_product },
    { path: '/products/:product_id', name:'products.show', component:show_product },

   
    //Customers
    { path: '/customers', name:'customers.index', component: customers },
    { path: '/customers/create', name:'customers.create', component: create_customer },
    { path: '/customers/:customer_id/edit', name:'customers.edit', component:create_customer },
    { path: '/customers/:customer_id', name:'customers.show', component:show_customer },

    //Bills
    { path: '/bills', name:'bills.index', component: bills },
    { path: '/bills/create', name:'bills.create', component: create_bill },
    { path: '/bills/:bill_id/edit', name:'bills.edit', component:create_bill },
    { path: '/bills/:bill_id', name:'bills.show', component:show_bill },
    { path: '/bills/:bill_id/preview', name:'bills.preview', component:preview_bill },


    //Invoices
    { path: '/invoices', name:'invoices.index', component: invoices },
    { path: '/invoices/create', name:'invoices.create', component: create_invoice },
    { path: '/invoices/:invoice_id/edit', name:'invoices.edit', component:create_invoice },
    { path: '/invoices/:invoice_id', name:'invoices.show', component:show_invoice },
    { path: '/invoices/:invoice_id/preview', name:'invoices.preview', component:preview_invoice },
    
    //Reports
    { path: '/report', name:'report', component: Report },
    { path: '/bill/report', name:'bill-report', component: bill_report },
    { path: '/bill/tax_report', name:'bill-tax-report', component: bill_tax },

    { path: '/invoice/report', name:'invoice-report', component: invoice_report },
    { path: '/invoice/tax_report', name:'invoice-tax-report', component: invoice_tax },


    { path: '/product/stock_report', name:'stock-report', component: stock_report },
   
];

const router = new VueRouter({
    // mode: 'history',
    routes
});

const app = new Vue({
    el: '#app',
    router,
    components: {Myheader, Mymenu, Myfooter},
    methods:{
        windowreload(){
            location.reload(true);
        }
    }
});

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{ asset('css/theme.min.css') }}" id="stylesheetLight">
    <style>body { display: none; }</style>
</head>
<body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-6 col-xl-4 px-lg-6 my-5">
                <h1 class="display-4 text-center mb-3">Password reset</h1>
                <p class="text-muted text-center mb-5">
                    Enter new password to password reset.
                </p>
                <form method="post" action="{{ route('password.update') }}" autocomplete="off">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="name@address.com" name="email" autofocus value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control form-control-appended{{ $errors->has('password') ? ' is-invalid' : '' }}"" placeholder="Enter new password" name="password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control form-control-appended{{ $errors->has('password') ? ' is-invalid' : '' }}"" placeholder="Confirm new password" name="password_confirmation">
                    </div>
                    <button method="submit" class="btn btn-lg btn-block btn-primary mb-3">
                        Reset Password
                    </button>
                    <div class="text-center">
                        <small class="text-muted text-center">
                            Remember your password? <a href="{{ route('login') }}">Log in</a>.
                        </small>
                    </div>
              </form>
            </div>
            <div class="col-12 col-md-7 col-lg-6 col-xl-8 d-none d-lg-block">
                <div class="bg-cover vh-100 mt-n1 mr-n3" style="background-image: url(../../storage/gold.jpg);"></div>
            </div>
        </div>
    </div>
</body>
</html>
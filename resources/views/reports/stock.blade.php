@if($request->display_type=="excel")
@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=stock-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
    <title>Stock Report</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
</head>
<body>
    <table border="1" width="100%">
        <thead>
            <tr>
                <td align="left" colspan="5"> 
                    <div>
                        <strong>{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
                <td align="left" colspan="5"> 
                	<div>
                        <strong>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}
                        -To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</strong>
                </div>
                <div> Category : @if($category){{ $category->category_name }} @else All @endif </div>
                <div> Subcategory : @if($subcategory){{ $subcategory->subcategory_name }}@else All @endif </div>
                </td>
            </tr>
            <tr >
                <th rowspan="2">#</th>
                <th class="text-center" rowspan="2">Product Name</th>
                <th colspan="2">Opening Stock</th>
                <th colspan="2">Purchase Stock</th>
                <th colspan="2">Sales Stock</th>
                <th colspan="2">Clossing Stock</th>
            </tr>
             <tr>
                <th>Pieces</th>
                <th>Net Wt</th>
                <th>Pieces</th>
                <th>Net Wt</th>
                <th>Pieces</th>
                <th>Net Wt</th>
                <th>Pieces</th>
                <th>Net Wt</th>
            </tr>
        </thead>
        @php
            $i=0;
            $pieces_opening_stock  = 0;
            $weight_opening_stock  = 0;
            $pieces_purchase_stock = 0;
            $weight_purchase_stock = 0;
            $pieces_indent_stock   = 0;
            $weight_indent_stock   = 0;
            $pieces_clossing_stock  = 0;
            $weight_clossing_stock  = 0;
        @endphp
        <tbody>
            @foreach($stock_products as $product)
                <tr>
                    <td class="text-center">{{ ++$i }}</td>
                    <td>{{ $product['product']['product_name'] }}</td>
                    <td>{{ number_format($product['pieces_opening_stock'],2) }}</td>
                    <td>{{ number_format($product['weight_opening_stock'],2) }}</td>
                    <td>{{ number_format($product['pieces_purchase_stock'],2) }}</td>
                    <td>{{ number_format($product['weight_purchase_stock'],2) }}</td>
                    <td>{{ number_format($product['pieces_indent_stock'],2) }}</td>
                    <td>{{ number_format($product['weight_indent_stock'],2) }}</td>
                    <td>{{ number_format($product['pieces_clossing_stock'],2) }}</td>
                    <td>{{ number_format($product['weight_clossing_stock'],2) }}</td>
                </tr>
                @php
                $pieces_opening_stock    += $product['pieces_opening_stock'];
	            $weight_opening_stock    += $product['weight_opening_stock'];
	            $pieces_purchase_stock   += $product['pieces_purchase_stock'];
	            $weight_purchase_stock   += $product['weight_purchase_stock'];
	            $pieces_indent_stock     += $product['pieces_indent_stock'];
	            $weight_indent_stock     += $product['weight_indent_stock'];
                $pieces_clossing_stock   += $product['pieces_clossing_stock'];
                $weight_clossing_stock   += $product['weight_clossing_stock'];
                @endphp
            @endforeach
            <tr>
                <th colspan="2" style="text-align:right;" > Total </th>
                <th >{{ number_format($pieces_opening_stock,2)  }}</th>
                <th >{{ number_format($weight_opening_stock,2) }}</th>
                <th >{{ number_format($pieces_purchase_stock,2)   }}</th>
                <th >{{ number_format($weight_purchase_stock,2)   }}</th>
                <th >{{ number_format($pieces_indent_stock,2) }}</th>
                <th >{{ number_format($weight_indent_stock,2) }}</th>
                <th >{{ number_format($pieces_clossing_stock,2) }}</th>
                <th >{{ number_format($weight_clossing_stock,2) }}</th>
            </tr>
        </tbody>
    </table>
</body>
</html>
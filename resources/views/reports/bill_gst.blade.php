@if($request->display_type=="excel")
@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=bill-gst-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
    <title>Bill</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gst_report.css') }}">
</head>
<body>
    <table width="100%" border="1">
        <tr>
            <th class="text-center" colspan="9">BILL GST REPORT</th>
        </tr>
        <tr>
            <td colspan="5">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        <tr>
            <th class="text-center">#</th>
            <th>Bill No</th>
            <th>Bill Date</th>
            <th>Due Date</th>
            <th>Supplier</th>
            <th>Supplier Code</th>
            <th>GSTIN</th>
            <th class="text-right">Grand Total</th>
        </tr>
        @php
            $i=0;
            $overall_total_amount=0;
            $overall_total_cgst=0;
            $overall_total_sgst=0;
            $overall_total_igst=0;
            $overall_total_sub=0;
        @endphp
        @foreach($bills as $bill)
            <tr>
                <th >{{ ++$i }}</th>
                <td >{{ $bill->bill_no }}</td>
                <td>{{  date('d-m-Y',strtotime($bill->bill_date)) }}</td>
                <td>{{  date('d-m-Y',strtotime($bill->due_date)) }}</td>
                <td>{{ $bill->Supplier->supplier_name }}</td>
                <td>{{ $bill->Supplier->supplier_code }}</td>
                <td>{{ $bill->Supplier->gstin_no }}</td>
                <td class="text-right">{{ number_format($bill->grand_total,2) }}</td>
            </tr>
            <tr>
                <th colspan="2" rowspan="2" class="text-center">#</th>
                <th rowspan="2">Taxable Amount</th>
                @if($bill->source_id == $bill->destination_id)
                <th colspan="2" class="text-center">CGST</th>
                <th colspan="2" class="text-center">SGST</th>
                @endif
                @if($bill->source_id != $bill->destination_id)
                <th colspan="4" class="text-center">IGST</th>
                @endif
                <th rowspan="2">Total Amount</th>
            </tr>
            <tr>
                @if($bill->source_id == $bill->destination_id)
                    <th>Rate</th>
                    <th>Amount</th>
                    <th>Rate</th>
                    <th>Amount</th>
                @endif
                @if($bill->source_id != $bill->destination_id)
                    <th colspan="2">Rate</th>
                    <th colspan="2">Amount</th>
                @endif
            </tr>
            @php
                $total_amount=$bill->grand_total-$bill->tax_amount;
            @endphp
            <tr>
                <td colspan="2" class="text-center">Total</td>
                <td class="text-right">{{ number_format($total_amount,2) }}</td>
                @if($bill->source_id == $bill->destination_id)
                    @php
                        $bill_cgst_amount=$bill->tax_amount/2;
                        $bill_sgst_amount=$bill->tax_amount/2;
                        $overall_total_cgst+=$bill_cgst_amount;
                        $overall_total_sgst+=$bill_sgst_amount;
                    @endphp
                    <td class="text-right">{{ $bill->Tax->cgst_name }}</td>
                    <td class="text-right">{{ number_format($bill_cgst_amount,2) }}</td>
                    <td class="text-right">{{ $bill->Tax->sgst_name }}</td>
                    <td class="text-right">{{ number_format($bill_sgst_amount,2) }}</td>
                @endif
                @if($bill->source_id != $bill->destination_id)
                    @php
                        $bill_igst_amount=$bill->tax_amount;
                        $overall_total_igst+=$bill_igst_amount;
                    @endphp
                    <td colspan="2" class="text-right">{{ $bill->Tax->igst_name }}</td>
                    <td colspan="2" class="text-right">{{ number_format($bill_igst_amount,2) }}</td>
                @endif
                <td class="text-right">{{ number_format($bill->grand_total,2) }}</td>
            </tr>
            @php
                $overall_total_amount+=$total_amount;
                $overall_total_sub+=$bill->grand_total;
            @endphp
            <tr>
                <td colspan="9"></td>
            </tr>
        @endforeach
        <tr>
            <th colspan="2" class="text-center">Overall Total</th>
            <td class="text-right">{{ number_format($overall_total_amount,2) }}</td>
            <td colspan="2" class="text-center"><b>IGST : </b>{{ number_format($overall_total_igst,2) }}</td>
            <td class="text-center"><b>CGST : </b>{{ number_format($overall_total_cgst,2) }}</td>
            <td  class="text-center"><b>SGST : </b> {{ number_format($overall_total_sgst,2) }}</td>
            <td class="text-right">{{ number_format($overall_total_sub,2) }}</td>
        </tr>
    </table>
    <htmlpagefooter name="page-footer">
        {{-- <span class="text-right">PAGE {PAGENO}</span> --}}
    </htmlpagefooter>
</body>
</html>
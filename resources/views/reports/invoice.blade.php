@if($request->display_type=="excel")
@php
    header("Content-type: application/Invoice.ms-excel");
    header("Content-Disposition: attachment;Filename=invoice-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
</head>
<body>
	<table width="100%" border="1">
		<tr>
			<th colspan="12" style="text-align: center;">INVOICE REPORT</th>
		</tr>
		<tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="6">
            	@if($customer!='')
            		<div>
                        <strong>{{ $customer->customer_name }}</strong>
                    </div>
                    <div>{{ $customer->billing_address }}</div>
                    <div>Phone: {{ $customer->mobile_no }}</div>
                    <div>GSTIN: {{ $customer->gstin_no }}</div>
            	@endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
			<tr>
	            <th class="text-center">#</th>
	            <th>Invoice No</th>
	            <th>Invoice Date</th>
	           	<th>Due Date</th>
	            <th>Customer</th>
                <th>Payment Term</th>
	            <th >Sub Total</th>
                <th >Discount</th>
                <th >Tax</th>
                <th >Total Amount</th>
	            <th >Round off</th>
	            <th >Grand Total</th>
	        </tr>
	        @php
	        	$i=0;
	        	$sub_total = 0;
	        	$discount_amount = 0;
	        	$tax_amount = 0;
	        	$total_amount = 0;
                $round_off = 0;
	        	$grand_total = 0;
	        @endphp
	        @foreach($invoices as $invoice)
		        <tr>
		            <td class="text-center">
		                {{ ++$i }}
		            </td>
		            <td>
		                {{ $invoice->invoice_no }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($invoice->due_date)) }}
		            </td>
		            <td>
		                {{ $invoice->Customer->customer_name }}
		            </td>
                    <td>
                        {{ $invoice->PaymentTerm->payment_term }}
                    </td>
		            <td >
		                {{ number_format($invoice->sub_total,2) }}
		            </td>
                    <td >
		            	{{ number_format($invoice->discount_amount,2) }}
		            </td>
                     <td >
                        {{ number_format($invoice->tax_amount,2) }}
                    </td>
                    <td >
                        {{ number_format($invoice->total_amount,2) }}
                    </td>
		            <td >
		            	{{ number_format($invoice->round_off,2) }}
		            </td>
		            <td >
		            	{{ number_format($invoice->grand_total,2) }}
		            </td>
		            @php
		            	$sub_total += $invoice->sub_total;
		            	$discount_amount += $invoice->discount_amount;
		            	$tax_amount += $invoice->tax_amount;
                        $total_amount += $invoice->total_amount;
		            	$round_off += $invoice->round_off;
		            	$grand_total += $invoice->grand_total;
		            @endphp
		        </tr>
		    @endforeach
		    <tr>
		    	<th style="text-align: right;" colspan="6" >Total</th>
		    	<th >{{ number_format($sub_total,2) }}</th>
                <th >{{ number_format($tax_amount,2) }}</th>
		    	<th >{{ number_format($discount_amount,2) }}</th>
                <th >{{ number_format($total_amount,2) }}</th>
		    	<th >{{ number_format($round_off,2) }}</th>
		    	<th >{{ number_format($grand_total,2) }}</th>
		    </tr>
		@endif
		@if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($invoices as $invoice)
                <tr>
                    <th colspan="10">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $invoice->Customer->customer_name }}</div>
                        <div>{{ $invoice->Customer->billing_address }}</div>
                        <div>{{ $invoice->Customer->shipping_address }}</div>
                        <div>{{ $invoice->Customer->email }}</div>
                        <div>{{ $invoice->Customer->mobile_no }}</div>
                        <div>{{ $invoice->Customer->pan_no }}</div>
                        <div>{{ $invoice->Customer->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>Invoice No:{{ $invoice->invoice_no }}</div>
                        <div>Invoice Date:{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</div>
                        <div>Due Date :{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</div>
                        <div>Ref No :{{ $invoice->reference_no }}</div>
                        <div>Ref Date : @if($invoice->reference_date!=null)
                        {{ date('d-m-Y',strtotime($invoice->reference_date)) }}
                        @else
                        @endif</div> 
                        <div>Payment Term : {{ $invoice->PaymentTerm->payment_term }}</div> 
                    </td>
                </tr>
                <tr>
                    <th >#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Net Wt</th>
                    <th>Pieces</th>
                    <th>Net Rate</th>
                    <th>Piece Rate</th>
                    <th>Amount</th>
                    <th>Box Amount</th>
                    <th>Sub Total</th>
                </tr>
                @php $j=0; @endphp
                @foreach($invoice->InvoiceProducts as $invoice_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $invoice_product->product_code }}
                        </td>
                        <td>
                            {{ $invoice_product->product_name }}
                        </td>
                        <td>
                            {{ $invoice_product->net_weight }}
                        </td>
                        <td>
                            {{ $invoice_product->pieces }}
                        </td>
                         <td>
                           {{ number_format($invoice_product->net_rate,2) }}
                        </td>
                        <td>
                           {{ number_format($invoice_product->piece_rate,2) }}
                        </td>
                        <td>
                            {{ number_format($invoice_product->amount,2) }}
                        </td>
                        <td>
                            {{ number_format($invoice_product->box_amount,2) }}
                        </td>
                     
                        <td>
                            {{ number_format($invoice_product->sub_total,2) }}
                        </td>                   
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="5" colspan="6">
                        <strong>Terms & Conditions : </strong><br>
                        {{ $invoice->terms }}
                    </td>
                    <th style="text-align: right;" colspan="2">Sub Total</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->sub_total,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Discount</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->discount_amount,2) }}
                    </th>
                </tr>
                 <tr>
                    <th style="text-align: right;" colspan="2">Shipping/Insurance</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->shipping_charges,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Hallmark Charges</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->hallmark_charges,2) }}
                    </th>
                </tr>
                 <tr>
                    <th style="text-align: right;" colspan="2">Packing/others</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->packing_charges,2) }}
                    </th>
                </tr>
                <tr>
                    <td rowspan="4" colspan="6">
                        <strong>Note : </strong><br>
                        {{ $invoice->note  }}
                    </td>
                    <th style="text-align: right;" colspan="2">Tax</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->tax_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Total</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Round off</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Grand Total</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($invoice->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
	</table>
</body>
</html>
                          
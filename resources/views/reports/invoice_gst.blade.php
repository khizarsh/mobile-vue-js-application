@if($request->display_type=="excel")
@php
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment;Filename=invoice-gst-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
    <title>Invoice</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gst_report.css') }}">
</head>
<body>
    <table width="100%" border="1">
        <tr>
            <th class="text-center" colspan="9">INVOICE GST REPORT</th>
        </tr>
        <tr>
            <td colspan="5">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="4">
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        <tr>
            <th class="text-center">#</th>
            <th>Invoice No</th>
            <th>Invoice Date</th>
            <th>Due Date</th>
            <th>Customer</th>
            <th>Customer Code</th>
            <th>GSTIN</th>
            <th class="text-right">Grand Total</th>
        </tr>
        @php
            $i=0;
            $overall_total_amount=0;
            $overall_total_cgst=0;
            $overall_total_sgst=0;
            $overall_total_igst=0;
            $overall_total_sub=0;
        @endphp
        @foreach($invoices as $invoice)
            <tr>
                <th >{{ ++$i }}</th>
                <td >{{ $invoice->invoice_no }}</td>
                <td>{{  date('d-m-Y',strtotime($invoice->invoice_date)) }}</td>
                <td>{{  date('d-m-Y',strtotime($invoice->due_date)) }}</td>
                <td>{{ $invoice->Customer->customer_name }}</td>
                <td>{{ $invoice->Customer->customer_code }}</td>
                <td>{{ $invoice->Customer->gstin_no }}</td>
                <td class="text-right">{{ number_format($invoice->grand_total,2) }}</td>
            </tr>
            <tr>
                <th colspan="2" rowspan="2" class="text-center">#</th>
                <th rowspan="2">Taxable Amount</th>
                @if($invoice->source_id == $invoice->destination_id)
                <th colspan="2" class="text-center">CGST</th>
                <th colspan="2" class="text-center">SGST</th>
                @endif
                @if($invoice->source_id != $invoice->destination_id)
                <th colspan="4" class="text-center">IGST</th>
                @endif
                <th rowspan="2">Total Amount</th>
            </tr>
            <tr>
                @if($invoice->source_id == $invoice->destination_id)
                    <th>Rate</th>
                    <th>Amount</th>
                    <th>Rate</th>
                    <th>Amount</th>
                @endif
                @if($invoice->source_id != $invoice->destination_id)
                    <th colspan="2">Rate</th>
                    <th colspan="2">Amount</th>
                @endif
            </tr>
            @php
                $total_amount=$invoice->grand_total-$invoice->tax_amount;
            @endphp
            <tr>
                <td colspan="2" class="text-center">Total</td>
                <td class="text-right">{{ number_format($total_amount,2) }}</td>
                @if($invoice->source_id == $invoice->destination_id)
                	@php
                		$invoice_cgst_amount=$invoice->tax_amount/2;
                		$invoice_sgst_amount=$invoice->tax_amount/2;
                		$overall_total_cgst+=$invoice_cgst_amount;
                		$overall_total_sgst+=$invoice_sgst_amount;
                	@endphp
                    <td class="text-right">{{ $invoice->Tax->cgst_name }}</td>
                    <td class="text-right">{{ number_format($invoice_cgst_amount,2) }}</td>
                    <td class="text-right">{{ $invoice->Tax->sgst_name }}</td>
                    <td class="text-right">{{ number_format($invoice_sgst_amount,2) }}</td>
                @endif
                @if($invoice->source_id != $invoice->destination_id)
		            @php
		                $invoice_igst_amount=$invoice->tax_amount;
		                $overall_total_igst+=$invoice_igst_amount;
		            @endphp
                    <td colspan="2" class="text-right">{{ $invoice->Tax->igst_name }}</td>
                    <td colspan="2" class="text-right">{{ number_format($invoice_igst_amount,2) }}</td>
                @endif
                <td class="text-right">{{ number_format($invoice->grand_total,2) }}</td>
            </tr>
            @php
                $overall_total_amount+=$total_amount;
                $overall_total_sub+=$invoice->grand_total;
            @endphp
            <tr>
                <td colspan="9"></td>
            </tr>
        @endforeach
        <tr>
            <th colspan="2" class="text-center">Overall Total</th>
            <td class="text-right">{{ number_format($overall_total_amount,2) }}</td>
            <td colspan="2" class="text-center"><b>IGST : </b>{{ number_format($overall_total_igst,2) }}</td>
            <td class="text-center"><b>CGST : </b>{{ number_format($overall_total_cgst,2) }}</td>
            <td  class="text-center"><b>SGST : </b> {{ number_format($overall_total_sgst,2) }}</td>
            <td class="text-right">{{ number_format($overall_total_sub,2) }}</td>
        </tr>
    </table>
    <htmlpagefooter name="page-footer">
        {{-- <span class="text-right">PAGE {PAGENO}</span> --}}
    </htmlpagefooter>
</body>
</html>
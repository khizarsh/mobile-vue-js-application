@if($request->display_type=="excel")
@php
    header("Content-type: application/Bill.ms-excel");
    header("Content-Disposition: attachment;Filename=bill-report.xls");
@endphp
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Bill</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
</head>
<body>
	<table width="100%" border="1">
		<tr>
			<th colspan="12" style="text-align: center;">BILL REPORT</th>
		</tr>
		<tr>
            <td colspan="6">
                <div>
                    <strong>{{ $org->org_name }}</strong>
                </div>
                <div>{{ $org->address }}</div>
                <div>Email: {{ $org->email }}</div>
                <div>Phone: {{ $org->mobile_no }}</div>
                <div>PAN: {{ $org->pan_no }}</div>
                <div>GSTIN: {{ $org->gstin_no }}</div>
            </td>
            <td colspan="6">
            	@if($supplier!='')
            		<div>
                        <strong>{{ $supplier->supplier_name }}</strong>
                    </div>
                    <div>{{ $supplier->billing_address }}</div>
                    <div>Phone: {{ $supplier->mobile_no }}</div>
                    <div>GSTIN: {{ $supplier->gstin_no }}</div>
            	@endif
                <div>From Date: {{ date('d-m-Y',strtotime($request->from_date)) }}</div>
                <div>To Date: {{ date('d-m-Y',strtotime($request->to_date)) }}</div>
            </td>
        </tr>
        @if($request->report_type == 'Brief Report')
			<tr>
	            <th class="text-center">#</th>
	            <th>Bill No</th>
	            <th>Bill Date</th>
	           	<th>Due Date</th>
	            <th>Supplier</th>
                <th>Payment Term</th>
	            <th >Sub Total</th>
                <th >Discount</th>
                <th >Tax</th>
                <th >Total Amount</th>
	            <th >Round off</th>
	            <th >Grand Total</th>
	        </tr>
	        @php
	        	$i=0;
	        	$sub_total = 0;
	        	$discount_amount = 0;
	        	$tax_amount = 0;
	        	$total_amount = 0;
                $round_off = 0;
	        	$grand_total = 0;
	        @endphp
	        @foreach($bills as $bill)
		        <tr>
		            <td class="text-center">
		                {{ ++$i }}
		            </td>
		            <td>
		                {{ $bill->bill_no }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($bill->bill_date)) }}
		            </td>
		            <td>
		                {{ date('d-m-Y',strtotime($bill->due_date)) }}
		            </td>
		            <td>
		                {{ $bill->Supplier->supplier_name }}
		            </td>
                    <td>
                        {{ $bill->PaymentTerm->payment_term }}
                    </td>
		            <td >
		                {{ number_format($bill->sub_total,2) }}
		            </td>
                    <td >
		            	{{ number_format($bill->discount_amount,2) }}
		            </td>
                     <td >
                        {{ number_format($bill->tax_amount,2) }}
                    </td>
                    <td >
                        {{ number_format($bill->total_amount,2) }}
                    </td>
		            <td >
		            	{{ number_format($bill->round_off,2) }}
		            </td>
		            <td >
		            	{{ number_format($bill->grand_total,2) }}
		            </td>
		            @php
		            	$sub_total += $bill->sub_total;
		            	$discount_amount += $bill->discount_amount;
		            	$tax_amount += $bill->tax_amount;
                        $total_amount += $bill->total_amount;
		            	$round_off += $bill->round_off;
		            	$grand_total += $bill->grand_total;
		            @endphp
		        </tr>
		    @endforeach
		    <tr>
		    	<th style="text-align: right;" colspan="6" >Total</th>
		    	<th >{{ number_format($sub_total,2) }}</th>
                <th >{{ number_format($tax_amount,2) }}</th>
		    	<th >{{ number_format($discount_amount,2) }}</th>
                <th >{{ number_format($total_amount,2) }}</th>
		    	<th >{{ number_format($round_off,2) }}</th>
		    	<th >{{ number_format($grand_total,2) }}</th>
		    </tr>
		@endif
		@if($request->report_type == 'Detail Report')
            @php
                $i=0;
            @endphp
            @foreach($bills as $bill)
                <tr>
                    <th colspan="10">{{ ++$i }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                         <div>{{ $bill->Supplier->customer_name }}</div>
                        <div>{{ $bill->Supplier->billing_address }}</div>
                        <div>{{ $bill->Supplier->shipping_address }}</div>
                        <div>{{ $bill->Supplier->email }}</div>
                        <div>{{ $bill->Supplier->mobile_no }}</div>
                        <div>{{ $bill->Supplier->pan_no }}</div>
                        <div>{{ $bill->Supplier->gstin_no }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div>bill No:{{ $bill->bill_no }}</div>
                        <div>bill Date:{{ date('d-m-Y',strtotime($bill->bill_date)) }}</div>
                        <div>Due Date :{{ date('d-m-Y',strtotime($bill->bill_date)) }}</div>
                        <div>Ref No :{{ $bill->reference_no }}</div>
                        <div>Ref Date : @if($bill->reference_date!=null)
                        {{ date('d-m-Y',strtotime($bill->reference_date)) }}
                        @else
                        @endif</div> 
                        <div>Payment Term : {{ $bill->PaymentTerm->payment_term }}</div> 
                    </td>
                </tr>
                <tr>
                    <th >#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Net Wt</th>
                    <th>Pieces</th>
                    <th>Net Rate</th>
                    <th>Piece Rate</th>
                    <th>Amount</th>
                    <th>Box Amount</th>
                    <th>Sub Total</th>
                </tr>
                @php $j=0; @endphp
                @foreach($bill->BillProducts as $bill_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $bill_product->product_code }}
                        </td>
                        <td>
                            {{ $bill_product->product_name }}
                        </td>
                        <td>
                            {{ $bill_product->net_weight }}
                        </td>
                        <td>
                            {{ $bill_product->pieces }}
                        </td>
                         <td>
                           {{ number_format($bill_product->net_rate,2) }}
                        </td>
                        <td>
                           {{ number_format($bill_product->piece_rate,2) }}
                        </td>
                        <td>
                            {{ number_format($bill_product->amount,2) }}
                        </td>
                        <td>
                            {{ number_format($bill_product->box_amount,2) }}
                        </td>
                     
                        <td>
                            {{ number_format($bill_product->sub_total,2) }}
                        </td>                   
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="5" colspan="6">
                        <strong>Terms & Conditions :</strong><br>
                        {{ $bill->terms }}
                    </td>
                    <th style="text-align: right;" colspan="2">Sub Total</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->sub_total,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Discount</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->discount_amount,2) }}
                    </th>
                </tr>
                 <tr>
                    <th style="text-align: right;" colspan="2">Shipping/Insurance</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->shipping_charges,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Hallmark Charges</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->hallmark_charges,2) }}
                    </th>
                </tr>
                 <tr>
                    <th style="text-align: right;" colspan="2">Packing/others</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->packing_charges,2) }}
                    </th>
                </tr>
                <tr>
                    <td rowspan="4" colspan="6">
                        <strong>Note : </strong><br>
                        {{ $bill->note  }}
                    </td>
                    <th style="text-align: right;" colspan="2">Tax</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->tax_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Total</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Round off</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: right;" colspan="2">Grand Total</th>
                    <th style="text-align: right;" colspan="2">
                        {{ number_format($bill->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
	</table>
</body>
</html>

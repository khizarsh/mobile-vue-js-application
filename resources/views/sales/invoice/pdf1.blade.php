<!DOCTYPE html>
<html>
<head>
    <title>{{ $invoice->invoice_no }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100 table-border">
                 <tr>
                <td width="17%">
                    <img src="{{asset('storage/organization')}}/{{ $org->logo }}" width="15%" alt="product image">
                </td>
                <td width="60%" align="left" style="padding-top: 2%;">
                    <b>{{ $org->org_name }}</b><br>
                    <b>Email:</b> {{ $org->email }} <br>
                    <b>Phone No:</b> {{ $org->phone_no }} <br>
                    <b>Mobile No :</b> {{ $org->mobile_no }} <br>
                    <b>Address : </b> {{ $org->address }}<br>
                 </td>
                <td align="right" style="padding-top: 2%;">
                    <b>GSTIN : {{ $org->gstin_no }}</b><br>
                    <b>PAN NO : {{ $org->pan_no }}</b>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4"><span><h2>QUOTATION</h2></span></td>
            </tr>
        </table>
    </htmlpageheader>
    <table class="table-100" border="1">
        <tr>
            <td rowspan="3">
                <span><b>To</b></span>
                <br/><span><b>{{ $invoice->Customer->customer_name }}</b></span>
                <br/><span><b>Phone :</b> {{ $invoice->Customer->mobile_no }}</span>
                <br/><span><b>Email :</b> {{ $invoice->Customer->email }}</span>
                <br/><span><b>Billing Address :</b>{{ $invoice->Customer->billing_address }}</span>
                <br/><span><b>Shipping Address :</b>{{ $invoice->Customer->shipping_address }}</span>
                <br/><span><b>GSTIN :</b> {{ $invoice->Customer->gstin_no }}</span>
            </td>
            <td><b> Invoice No : </b><br>{{ $invoice->invoice_no }}</td>
            <td><b> Invoice Date:</b> <br>{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</td>
        </tr>
        <tr>
            <td><b>Ref No: </b><br>{{ $invoice->reference_no }}</td>
            <td><b>Ref Date:</b><br> {{ date('d-m-Y',strtotime($invoice->reference_date)) }}</td>
        </tr>
        <tr>
            <td><b>Source:</b><br> {{ $invoice->SourcePlace->place_name }}</td>
            <td><b>Destination:</b><br> {{ $invoice->DestinationPlace->place_name }}</td>
        </tr>
    </table>
    <table class="table-border table-100 th-font" border="1">
        <tr>
            <th align="center">#</th>
            <th >Category</th>
            <th >Subcategory</th>
            <th >Product</th>
            <th >Gross Wt</th>
            <th >Cover Wt</th>
            <th >Net Wt</th>
            <th >Pieces</th>
            <th >Silver Rate</th>
            <th >Making Chrg</th>
            <th >Net Rate</th>
            <th >Piece Rate</th>
            <th >Amount</th>
            <th >Other</th>
            <th >Box Code</th>
            <th >Box Rate</th>
            <th >Box Amount</th>
            <th >Sub Total</th>
        </tr>
        @php 
            $i = 1;
            $j=0; 
        @endphp
        @foreach($invoice->InvoiceProducts as $key => $invoice_product)
            <tr>
                <td align="center">{{ $key+1 }}</td>
                <td >{{ $invoice_product->Category['category_name']}}</td> 
                <td >{{ $invoice_product->Subcategory['subcategory_name']}}</td> 
                <td align="center">
                    <img src="{{asset('storage/products')}}/{{ $invoice_product->Product->latestPrimaryImage['image'] }}" height="60px" widtd="15%" alt="product image">
                    <br>
                    {{ $invoice_product->product_name }}</td>
                <td align="center" >{{ $invoice_product->gross_weight }}</td>
                <td align="center">{{ $invoice_product->cover_weight }}</td>
                <td align="center" >{{ $invoice_product->net_weight }}</td>
                <td align="center" >{{ $invoice_product->pieces }}</td>
                <td align="right" >{{ number_format($invoice_product->silver_rate,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->making_charges,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->net_rate,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->piece_rate,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->amount,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->other_charges,2) }}</td>
                <td >{{ $invoice_product->box_code }}</td>
                <td align="right" >{{ number_format($invoice_product->box_rate,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->box_amount,2) }}</td>
                <td align="right" >{{ number_format($invoice_product->sub_total,2) }}</td>
            </tr>
        @endforeach
        <tr>
            <td rowspan="5" colspan="14">
                <strong>Terms and Conditions : </strong><br>
                {{ $invoice->terms }}
            </td>
            <th align="right" colspan="2">Sub Total</th>
            <th align="right" colspan="2">{{ number_format($invoice->sub_total,2) }}</th>
        </tr>
        <tr>
            <th align="right" colspan="2">Discount</th>
            <th align="right" colspan="2">{{ number_format($invoice->discount_amount,2) }}</th>
        </tr>
        <tr>
            <th align="right" colspan="2">Shipping/Insurance</th>
            <th align="right" colspan="2">{{ number_format($invoice->shipping_charges,2) }}</th>
        </tr>
        <tr>
            <th align="right" colspan="2">Hallmark Charges</th>
            <th align="right" colspan="2">{{ number_format($invoice->hallmark_charges,2) }}</th>
        </tr>
        <tr>
             <th align="right"  colspan="2">Packing/others</th>
            <th align="right"  colspan="2">{{ number_format($invoice->packing_charges,2) }}</th>
        </tr>
         @php
            $basic_amount = $invoice->sub_total-$invoice->discount_amount+$invoice->shipping_charges+$invoice->shipping_charges+$invoice->hallmark_charges;
                $igst_rate = $invoice->Tax->igst_name;
                $sgst_rate = $invoice->Tax->sgst_name;
                $cgst_rate = $invoice->Tax->cgst_name;

            if ($invoice->source_id != $invoice->destination_id) {
                $igst_amount = round(($basic_amount * $invoice->Tax->igst_rate)/100,2);
                $sgst_amount = 0;
                $cgst_amount = 0;
            }else{
                $igst_amount = 0;
                $sgst_amount = round(($basic_amount * $invoice->Tax->sgst_rate)/100,2);
                $cgst_amount = round(($basic_amount * $invoice->Tax->cgst_rate)/100,2);
            }
        @endphp
        <tr> 
            <td rowspan="5" colspan="14">
                <strong>Note : </strong><br>
                {{ $invoice->note }}
            </td>
            <th align="right"  colspan="2">SGST @ {{ $sgst_rate }} </th>
            <th align="right"  colspan="2">{{ number_format($sgst_amount,2) }}</th>
        </tr>
       
        {{-- @if($invoice->source_id != $invoice->destination_id) --}}
       
        {{-- @else --}}
        <tr>
            <th align="right"  colspan="2">CGST @ {{ $cgst_rate }}</th>
            <th align="right"  colspan="2">{{ number_format($cgst_amount,2) }}</th>
        </tr>
        <tr>
            <th align="right"  colspan="2">IGST @ {{ $igst_rate }}</th>
            <th align="right"  colspan="2">{{ number_format($igst_amount,2) }}</th>
        </tr>
        {{-- @endif --}}
        {{-- <tr>
            <th align="right"  colspan="2">Tax</th>
            <th align="right"  colspan="2">{{ number_format($invoice->tax_amount,2) }}</th>
        </tr> --}}
         <tr>
            <th align="right"  colspan="2">Round Off</th>
            <th align="right"  colspan="2">{{ number_format($invoice->round_off,2) }}</th>
        </tr>
        <tr>
            <th align="right"  colspan="2">Grand Total</th>
            <th align="right"  colspan="2">{{ number_format($invoice->grand_total,2) }}</th>
        </tr>
    </table>
    <htmlpagefooter name="page-footer">
        <table class="table-100">
            <tr>
                <td align="left">This is a computer generated Invoice and does not require seal and signature.</td>
                <td align="right">PAGE {PAGENO}</td>
            </tr>
        </table>
    </htmlpagefooter>
</body>
</html>

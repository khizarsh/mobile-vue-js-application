<!DOCTYPE html>
<html>
<head>
	<title>{{ $invoice->invoice_no }}</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/invoice_pdf.css') }}">
</head>
<body>
	<htmlpageheader name="page-header">
		<h3 class="text-center">Tax Invoice</h3>
	</htmlpageheader>
	<table class="table">
		<tr>
			<td width="50%" rowspan="3">
				<img src="{{asset('storage/organization')}}/{{ $org->logo }}" width="15%" alt="product image">
				<br>				
				<strong>{{ $org->org_name }}</strong>
				<p>{{ $org->address }}</p>
				
				<p>GSTIN/UIN : {{ $org->gstin_no }}</p>
				
				<p>Email : {{ $org->email }}</p>
				<p>Phone : {{ $org->mobile_no }} | {{ $org->phone_no }}</p>
				
			</td>
			<td width="25%">
				Invoice No <br> 
				<strong>{{ $invoice->invoice_no }}</strong>
			</td>
			<td width="25%">
				Dated <br> 
				<strong>{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</strong>
			</td>
		</tr>
		<tr>
			<td>
				Reference No <br> 
				<strong>{{ $invoice->reference_no }}</strong>
			</td>
			<td>
				Reference Date <br> 
				<strong>{{ date('d-m-Y',strtotime($invoice->reference_date)) }}</strong>
			</td>
		</tr>
		<tr>
			<td>
				Source <br> 
				<strong>{{ $invoice->SourcePlace->place_name }}</strong>
			</td>
			<td>
				Destination <br> 
				<strong>{{ $invoice->DestinationPlace->place_name }}</strong>
			</td>
		</tr> 
		<tr>
			<td rowspan="2">
				<p>To</p>
				<strong>{{ $invoice->Customer->customer_name }}</strong>
				@if($invoice->Customer->billing_address!='')
					<p>Billing Address : </p>
					<p>{{ $invoice->Customer->billing_address }}</p>
				@endif
				@if($invoice->Customer->shipping_address!='')
					<p>Shipping Address : </p>
					<p>{{ $invoice->Customer->shipping_address }}</p>
				@endif
				<p>GSTIN/UIN : {{ $invoice->Customer->gstin_no }}</p>
				<p>State Name : {{ $invoice->DestinationPlace->place_name }}, Code : </p> 
			</td>
			<td colspan="2">
				Terms of Invoice<br>
				<strong>{{ $invoice->note }}</strong>
			</td>
		</tr>
		
	</table>


	<table class="grade-table">
		<tr>
			<tr>
                <th class="text-center">#</th>
                <th>Product Name</th>
                <th>Net Wt</th>
                <th>Pieces</th>
                <th>Net Rate</th>
                <th>Piece Rate</th>
                <th>Amount</th>
                <th>Box Amount</th>
                <th>Sub Total</th>
            </tr>
		</tr>
		@foreach($invoice->InvoiceProducts as $key => $invoice_product)
			<tr>
				<td class="text-center">{{ $key+1 }}</td>
				<td><strong>{{ $invoice_product->product_name }}</strong></td>
				<td class="text-center">{{ $invoice_product->net_weight }}</td>
				<td class="text-center">{{ $invoice_product->pieces }}</td>
				<td class="text-right"><strong>{{ number_format($invoice_product->net_rate,2) }}</strong></td>
				<td class="text-right">{{ number_format($invoice_product->piece_rate,2) }}</td>
				<td class="text-right"><strong>{{ number_format($invoice_product->amount,2) }}</strong></td>
				<td class="text-right"><strong>{{ number_format($invoice_product->box_amount,2) }}</strong></td>
				<td class="text-right"><strong>{{ number_format($invoice_product->sub_total,2) }}</strong></td>
			</tr>
		@endforeach
		@if($invoice->discount_amount!=0)
			<tr>
				<td class="text-right" colspan="8"><strong class="font-italic">Discount Amount</strong></td>
				<td class="text-right"><strong>{{ number_format($invoice->discount_amount,2) }}</strong></td>
			</tr>
		@endif
		<!-- <tr>
			<td></td>
			<td class="text-right">
				<strong class="font-italic">Output SGST 2.5%</strong>
			</td>
			<td></td>
			<td></td>
			<td></td>
			<td class="text-right">
				<strong>0.00</strong>
			</td>
		</tr> -->
		 @php
            $basic_amount = $invoice->sub_total-$invoice->discount_amount+$invoice->shipping_charges+$invoice->shipping_charges+$invoice->hallmark_charges;
                $igst_rate = $invoice->Tax->igst_name;
                $sgst_rate = $invoice->Tax->sgst_name;
                $cgst_rate = $invoice->Tax->cgst_name;

            if ($invoice->source_id != $invoice->destination_id) {
                $igst_amount = round(($basic_amount * $invoice->Tax->igst_rate)/100,2);
                $sgst_amount = 0;
                $cgst_amount = 0;
            }else{
                $igst_amount = 0;
                $sgst_amount = round(($basic_amount * $invoice->Tax->sgst_rate)/100,2);
                $cgst_amount = round(($basic_amount * $invoice->Tax->cgst_rate)/100,2);
            }
        @endphp
        <tr>
			<td class="text-right" colspan="8"><strong class="font-italic">SGST@ {{$sgst_rate}}</strong></td>
			<td class="text-right"><strong>{{ number_format($sgst_amount,2) }}</strong></td>
		</tr>
		<tr>
			<td class="text-right" colspan="8"><strong class="font-italic">CGST@ {{$cgst_rate}}</strong></td>
			<td class="text-right"><strong>{{ number_format($cgst_amount,2) }}</strong></td>
		</tr>
		<tr>
			<td class="text-right" colspan="8"><strong class="font-italic">IGST @ {{$igst_rate}}</strong></td>
			<td class="text-right"><strong>{{ number_format($igst_amount,2) }}</strong></td>
		</tr>
		{{-- <tr>
			<td class="text-right" colspan="8"><strong class="font-italic">Goods & Service Tax</strong></td>
			<td class="text-right"><strong>{{ number_format($invoice->tax_amount,2) }}</strong></td>
		</tr> --}}
		@if($invoice->shipping_charges!=0)
			<tr>
				<td class="text-right" colspan="8"><strong class="font-italic">Shipping/Insurance</strong></td>
				<td class="text-right"><strong>{{ number_format($invoice->shipping_charges,2) }}</strong></td>
			</tr>
		@endif
		@if($invoice->hallmark_charges!=0)
			<tr>
				<td class="text-right" colspan="8"><strong class="font-italic">Hallmark Charges</strong></td>
				<td class="text-right"><strong>{{ number_format($invoice->hallmark_charges,2) }}</strong></td>
			</tr>
		@endif
		@if($invoice->packing_charges!=0)
			<tr>
				<td class="text-right" colspan="8"><strong class="font-italic">Packing/others</strong></td>
				<td class="text-right"><strong>{{ number_format($invoice->packing_charges,2) }}</strong></td>
			</tr>
		@endif
		<tr>
			<th class="text-right" colspan="8">Round Off</th>
			<th class="text-right"> {{ number_format($invoice->round_off,2) }}</th>
		</tr>
		<tr>
			<th class="text-right" colspan="8">Grand Total</th>
			<th class="text-right">₹ {{ number_format($invoice->grand_total,2) }}</th>
		</tr>
		<tr>
			<td colspan="9">
				<p>Amount Chargeable (in words) :</p>
				<strong>INR {{ convert_number_to_words(round($invoice->grand_total)) }} ONLY</strong>
			</td>
		</tr>
	</table>
	<htmlpagefooter name="page-footer">
		<p class="text-center">This is a Computer Generated Invoice</p>
	</htmlpagefooter>
</body>
</html>
@php
    function convert_number_to_words($no)
    {   
        $words = array('0'=> 'ZERO' ,'1'=> 'ONE' ,'2'=> 'TWO' ,'3' => 'THREE','4' => 'FOUR','5' => 'FIVE','6' => 'SIX','7' => 'SEVEN','8' => 'EIGHT','9' => 'NINE','10' => 'TEN','11' => 'ELEVEN','12' => 'TWELVE','13' => 'THIRTEEN','14' => 'FOURTEEN','15' => 'FIFTEEN','16' => 'SIXTEEN','17' => 'SEVENTEEN','18' => 'EIGHTEEN','19' => 'NINETEEN','20' => 'TWENTY','30' => 'THIRTY','40' => 'FOURTY','50' => 'FIFTY','60' => 'SIXTY','70' => 'SEVENTY','80' => 'EIGHTY','90' => 'NINTY','100' => 'HUNDRED','1000' => 'THOUSAND','100000' => 'LAKH','10000000' => 'CRORE');
        if($no == 0)
        {
            return '';
        }
        else 
        {
            $novalue='';
            $highno=$no;
            $remainno=0;
            $value=100;
            $value1=1000;       
            while($no>=100)    
            {
                if(($value <= $no) &&($no  < $value1))    
                {
                    $novalue=$words["$value"];
                    $highno = (int)($no/$value);
                    $remainno = $no % $value;
                    break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
            if(array_key_exists("$highno",$words))
            {
                return $words["$highno"]." ".$novalue." ".convert_number_to_words($remainno);
            }
            else 
            {
                $unit=$highno%10;
                $ten =(int)($highno/10)*10;            
                return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".convert_number_to_words($remainno);
            }
        }
    }
@endphp
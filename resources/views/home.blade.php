<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="STOCK-BOOK" />  
        <meta name="author" content="MDBS Tech Private Limited">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="{{ asset('storage/favicon.ico') }}" rel="shortcut icon" type="image/png">
        <link href="{{ asset('vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        {{-- <link href="{{ asset('sidebar/css/main.css') }}" rel="stylesheet"> --}}
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
      
    </head>
    <body class="app header-fixed footer-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        <div id="app">
            <Myheader :org="{{ $org }}"  :user="{{ Auth::User() }}"></Myheader>
            <div class="app-body">
                <Mymenu :org="{{ $org }}"  :user="{{ Auth::User() }}"></Mymenu>
                <router-view :org="{{ $org }}"  :user="{{ Auth::User() }}"></router-view>
             <Myfooter :org="{{ $org }}" ></Myfooter>
            </div>
           
        </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="{{ asset('sidebar/js/main.js') }}"></script>

        <script src="{{ asset('vendors/jquery/js/jquery.min.js') }}"></script>
        <script src="{{ asset('vendors/pace-progress/js/pace.min.js') }}"></script>
        <script src="{{ asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('vendors/@coreui/coreui/js/coreui.min.js') }}"></script>
    </body>
</html>

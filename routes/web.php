<?php
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/copy','HomeController@copy');
Route::get('/copy_primary_images','HomeController@copy_primary_images');
Route::get('/copy_secondary_images','HomeController@copy_secondary_images');
Route::get('/copy_product_prices','HomeController@copy_product_prices');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

//Dashboard
Route::post('/data/view', 'HomeController@get_dashboard_data');
Route::post('/home/branch_view', 'HomeController@branch_view');

//Organization
Route::post('/organization/get_organization', 'OrganizationController@get_organization');
Route::post('/organization/update', 'OrganizationController@update');

//Preferences
Route::post('/preference/get_preference', 'PreferenceController@get_preference');
Route::post('/preference/update', 'PreferenceController@update');

//Taxes
Route::post('/tax/display', 'TaxController@display');
Route::post('/tax/store', 'TaxController@store');
Route::post('/tax/update', 'TaxController@update');
Route::post('/tax/destroy/{tax}', 'TaxController@destroy');
Route::post('/get_taxes', 'TaxController@get_taxes');

//Banks
Route::post('/bank/display', 'BankController@display');
Route::post('/bank/store', 'BankController@store');
Route::post('/bank/destroy/{bank}', 'BankController@destroy');
Route::post('/bank/update', 'BankController@update');
Route::post('/get_banks', 'BankController@get_banks');

//Terms
Route::post('/term/display', 'TermController@display');
Route::post('/term/store', 'TermController@store');
Route::post('/term/destroy/{term}', 'TermController@destroy');
Route::post('/term/update', 'TermController@update');
Route::post('/get_terms', 'TermController@get_terms');

//Payment Terms
Route::post('/payment_term/display', 'PaymentTermController@display');
Route::post('/payment_term/store', 'PaymentTermController@store');
Route::post('/payment_term/destroy/{payment_term}', 'PaymentTermController@destroy');
Route::post('/payment_term/update', 'PaymentTermController@update');
Route::post('/get_payment_terms', 'PaymentTermController@get_payment_terms');
Route::post('/payment_term/change_date', 'PaymentTermController@change_date');

//Place
Route::post('/place/display', 'PlaceController@display');
Route::post('/place/store', 'PlaceController@store');
Route::post('/place/destroy/{place}', 'PlaceController@destroy');
Route::post('/place/update', 'PlaceController@update');
Route::post('/get_places', 'PlaceController@get_places');

//Users
Route::post('/user/display', 'UserController@display');
Route::post('/user/store', 'UserController@store');
Route::post('/user/edit/{user}', 'UserController@edit');
Route::post('/user/update', 'UserController@update');
Route::post('/user/destroy/{user}', 'UserController@destroy');
Route::post('/profile/get_users', 'UserController@get_users')->name('profile.get_users');
Route::post('/profile/update', 'UserController@update_profile')->name('profile.update');
Route::post('/change/password', 'UserController@change_password');
Route::post('/get_limit_users', 'UserController@get_limit_users');
Route::post('save_bulk_assignment','ReferenceController@save_bulk_assignment');

//References
Route::ApiResource('/references', 'ReferenceController');
Route::get('/get_employee_leads', 'ReferenceController@get_employee_leads');
Route::get('/get_employee_manages', 'ReferenceController@get_employee_manages');
Route::get('/get_employee_customers', 'ReferenceController@get_employee_customers');

Route::ApiResource('/manages', 'ManageController');

//Employees
Route::ApiResource('/employees', 'EmployeeController');
Route::post('/get_employees', 'EmployeeController@get_employees');
Route::post('/get_limit_employees', 'EmployeeController@get_limit_employees');

//Classifications
Route::ApiResource('/classifications', 'ClassificationController');
Route::post('/get_classifications', 'ClassificationController@get_classifications');
Route::post('/get_limit_classifications', 'ClassificationController@get_limit_classifications');

//Categories
Route::ApiResource('/categories', 'CategoryController');
Route::get('/get_categories', 'CategoryController@get_categories');
Route::post('/get_limit_categories', 'CategoryController@get_limit_categories');

//Subcategories
Route::ApiResource('/subcategories', 'SubcategoryController');
Route::post('/get_subcategories', 'SubcategoryController@get_subcategories');
Route::post('/get_limit_subcategories', 'SubcategoryController@get_limit_subcategories');

//Masters
Route::ApiResource('/masters', 'MasterController');
Route::post('/get_masters', 'MasterController@get_masters');

//Suppliers
Route::ApiResource('/suppliers', 'SupplierController');
Route::post('/get_suppliers', 'SupplierController@get_suppliers');
Route::post('/get_supplier_code', 'SupplierController@get_supplier_code');
Route::post('/get_limit_suppliers', 'SupplierController@get_limit_suppliers');

//Products
Route::ApiResource('/products', 'ProductController');
Route::post('/get_product_code', 'ProductController@get_product_code');
Route::post('/get_products', 'ProductController@get_products');
Route::post('/product_image', 'ProductController@product_image');
Route::post('/product_image_delete', 'ProductController@product_image_delete');
Route::post('/get_limit_products', 'ProductController@get_limit_products');
Route::post('/get_latest_product_price', 'ProductController@get_latest_product_price');

Route::get('/product/catalog', 'ProductController@product_catalog');
Route::post('/get_barcode_qrcode', 'ProductController@get_barcode_qrcode');
Route::post('/products/import/excel', 'ProductController@import');


// Product Price
Route::ApiResource('/product_prices','ProductPriceController');
Route::post('/product_price_validation', 'ProductController@product_price_validation');

//Leads
Route::ApiResource('/leads', 'LeadController');
Route::post('/get_leads', 'LeadController@get_leads');
Route::post('/check_lead', 'LeadController@check_lead');
Route::post('/get_limit_leads', 'LeadController@get_limit_leads');
Route::post('/leads/import/excel', 'LeadController@import');
Route::get('/lead/report', 'LeadController@lead_report');


//Customers
Route::ApiResource('/customers', 'CustomerController');
Route::post('/get_customers', 'CustomerController@get_customers');
Route::post('/get_customer_code', 'CustomerController@get_customer_code');
Route::post('/customer_document', 'CustomerController@customer_document');
Route::post('/customer_document_delete', 'CustomerController@customer_document_delete');
Route::post('/customer_active', 'CustomerController@customer_active');
Route::post('/get_limit_customers', 'CustomerController@get_limit_customers');
Route::post('/customers/import/excel', 'CustomerController@import');
Route::get('/customer/report', 'CustomerController@customer_report');


//product add and update validation for purchase and sales
Route::post('/product/validation', 'ProductController@validation');

Route::post('/auto_generate_no', 'ProductController@auto_generate_no');

//Bills
Route::ApiResource('/bills', 'BillController');
Route::get('/bills/preview/{bill}', 'BillController@preview');
Route::post('/get_unpaid_bills', 'BillController@get_unpaid_bills');

//Payments
Route::ApiResource('/payments', 'PaymentController');
Route::get('/payments/preview/{payment}', 'PaymentController@preview');

//Quotations
Route::ApiResource('/quotations', 'QuotationController');
Route::get('/quotations/preview/{quotation}/{display}', 'QuotationController@preview');
Route::post('/quotation/invoice', 'QuotationController@convert_quotation');
Route::post('/quotation_mail', 'QuotationController@quotation_mail');


//Invoices
Route::ApiResource('/invoices', 'InvoiceController');
Route::get('/invoices/preview/{invoice}/{display}', 'InvoiceController@preview');
Route::post('/invoice_mail', 'InvoiceController@invoice_mail');
Route::post('/get_unpaid_invoices', 'InvoiceController@get_unpaid_invoices');
Route::post('/indent/invoice', 'InvoiceController@convert_indent');
Route::post('/indent/quotation', 'InvoiceController@convert_indent');
Route::post('/indent/proforma', 'InvoiceController@convert_indent');


Route::post('/proforma/invoice', 'InvoiceController@convert_proforma');
Route::post('/quotation/proforma', 'QuotationController@convert_quotation');

//Receipts
Route::ApiResource('/receipts', 'ReceiptController');
Route::get('/receipts/preview/{receipt}', 'ReceiptController@preview');

//Sessions
Route::ApiResource('/sessions', 'SessionController');

//Indents
Route::ApiResource('/indents', 'IndentController');
Route::post('/indent_product_validation', 'IndentController@indent_product_validation');
Route::get('/indents/preview/{indent}', 'IndentController@preview');
Route::get('/image_preview', 'IndentController@image_preview');
Route::post('/indent/repeat', 'IndentController@indent_repeat');


//Sales Staff
Route::ApiResource('/sliders', 'SliderController');
Route::post('/get_sliders', 'SliderController@get_sliders');
Route::ApiResource('/todos', 'TodoController');

//Likes
Route::ApiResource('/likes','LikeController');

//Proforma
Route::ApiResource('/proformas', 'ProformaController');
Route::get('/proformas/preview/{proforma}/{display}', 'ProformaController@preview');

//Notifications
Route::ApiResource('/notifications', 'NotificationController');
// Route::post('/get_notification', 'NotificationController@get_notification');

//Reports
Route::get('/stock_report', 'ReportController@stock_report');
Route::get('/reorder_level_report', 'ReportController@reorder_level_report');

Route::get('/invoice/report', 'ReportController@invoice_report');
Route::get('/invoice/tax_report', 'ReportController@invoice_tax_report');

Route::get('/bill/report', 'ReportController@bill_report');
Route::get('/bill/tax_report', 'ReportController@bill_tax_report');

Route::get('/quotation/report', 'ReportController@quotation_report');
Route::get('/proforma/report', 'ReportController@proforma_report');
Route::get('/indent/report', 'ReportController@indent_report');

Route::get('/report/ledger_report', 'ReportController@ledger_report');
Route::get('/report/payment_receipt', 'ReportController@payment_receipt');






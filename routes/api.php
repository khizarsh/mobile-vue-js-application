<?php

Route::group([
    'middleware' => 'api',
    'guard' => 'api',
    'prefix' => 'v1'

], function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('logout', 'API\AuthController@logout');
    Route::post('refresh', 'API\AuthController@refresh');
    Route::post('me', 'API\AuthController@me');
    Route::post('update_profile', 'API\AuthController@update_profile');
    Route::post('change_password', 'API\AuthController@change_password');

    //Product
    Route::apiResource('/products','API\ProductController');
    Route::post('/get_category_products','API\ProductController@get_category_products');
    Route::post('/sort_products', 'API\ProductController@sort_products');
    //Lead
    Route::apiResource('/leads','API\LeadController');
    Route::post('/lead_sub_modules','API\LeadController@lead_sub_modules');

    //Customer
    Route::apiResource('/customers','API\CustomerController');
    Route::post('/customer_sub_modules','API\CustomerController@customer_sub_modules');
    Route::post('/customer_document','API\CustomerController@customer_document');
    Route::post('/customer_documents_delete','API\CustomerController@customer_documents_delete');

    //EmployeeAPI
    Route::apiResource('/employees','API\EmployeeApiController');

    //Session
    Route::apiResource('/sessions','API\SessionController');
    Route::post('/up_video','API\SessionController@up_video');

    //Slider
    Route::apiResource('/sliders','API\SliderController');
    Route::get('/slider/new_collections','API\SliderController@new_collections');
    Route::get('/slider/top_selling_products','API\SliderController@top_selling_products');
    Route::get('/slider/all','API\SliderController@all');

    //Classification 
    Route::apiResource('/classifications','API\ClassificationController');
    //Categories
    Route::apiResource('/categories','API\CategoryController');

    //Likes
    Route::apiResource('/likes','API\LikeController');
    Route::post('/confirm_likes','API\LikeController@confirm_likes');
    Route::post('update_like_item','API\LikeController@update_like_item');
    Route::post('delete_like_item','API\LikeController@delete_like_item');
    Route::get('list_like_items','API\LikeController@list_like_items');

    //ToDo
    Route::apiResource('/todos','API\TodoController');
    Route::get('/get_todos','API\TodoController@get_todos');

    //Cart
    Route::post('add_to_cart','API\IndentController@add_to_cart');
    Route::post('update_cart_item','API\IndentController@update_cart_item');
    Route::post('delete_cart_item','API\IndentController@delete_cart_item');
    Route::get('list_cart_items','API\IndentController@list_cart_items');
    Route::post('indent_repeat', 'API\IndentController@indent_repeat');


    //Indents
    Route::apiResource('/indents','API\IndentController');

    //Notifications
    Route::apiResource('/notifications','API\NotificationController');

});

